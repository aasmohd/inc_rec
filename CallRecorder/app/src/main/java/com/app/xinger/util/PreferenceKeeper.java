package com.app.xinger.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceKeeper {

    private SharedPreferences prefs;
    private static PreferenceKeeper keeper;
    private static Context context;
    private static boolean appUsedReset;

    public static PreferenceKeeper getInstance(Context mContext) {
        if (keeper == null) {
            keeper = new PreferenceKeeper(mContext);
        }
        if(context == null){
            context = mContext.getApplicationContext();
        }
        return keeper;
    }

    private PreferenceKeeper(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setContext(Context context1) {
        context = context1;
    }

    public static void saveCookie(String cookie) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString("cookie", cookie).commit();
    }

    public static String getCookie() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString("cookie", "");
    }

    public static void clearCookie() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString("cookie", "").commit();
    }
}

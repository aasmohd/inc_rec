package com.app.xinger.util;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.app.xinger.db.annotation.NotRequiredForDB;
import com.app.xinger.db.annotation.Unique;
import com.app.xinger.dbhelper.DBClass;
import com.google.gson.Gson;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by tsingh on 20/2/15.
 */
public class DBUtil {

    private static final String LOG_TAG = DBUtil.class.getSimpleName();

    private static final String SET_STRING = "set";
    private static final String GET_STRING = "get";
    static final HashMap<String, String> varTypeMapping;
    static {

        varTypeMapping = new HashMap<String, String>();
        varTypeMapping.put("Boolean", "INTEGER");
        varTypeMapping.put("String", "TEXT");
        varTypeMapping.put("Integer", "INTEGER");
        varTypeMapping.put("Double", "REAL");
    }

    public static <T> ContentValues[] getContentValuesList(List<T> profiles) {
        ContentValues[] cv = new ContentValues[profiles.size()];
        for (int i = 0; i < profiles.size(); i++)
            cv[i] = getContentValues((DBClass) profiles.get(i));
        return cv;
    }


    public static ContentValues getContentValues(String key, String value) {
        ContentValues cv = new ContentValues();
        cv.put(key, value);
        return cv;
    }

    public static ContentValues getContentValues(DBClass object) {
        ContentValues cv = new ContentValues();
        Class<? extends DBClass> cl = object.getClass();
        Field[] fs = cl.getDeclaredFields();
        try {
            for (Field field : fs) {
                if(field.isAnnotationPresent(NotRequiredForDB.class)){
                    continue;
                }
                if (Modifier.isTransient(field.getModifiers())) {
                    continue;
                }
                String name = field.getName();
                Method method = cl.getMethod(GET_STRING + Character.toUpperCase(name.charAt(0)) + name.substring(1));
                Object value = method.invoke(object);
                if (value == null) {
                    continue;
                }
                if (TextUtils.equals(field.getType().getSimpleName(), "String")) {
                    cv.put(String.valueOf(field.getName()), (String) value);
                } else if (TextUtils.equals(field.getType().getSimpleName(), "Integer")) {
                    cv.put(String.valueOf(field.getName()), (Integer) value);
                } else if (TextUtils.equals(field.getType().getSimpleName(), "Long")) {
                    cv.put(String.valueOf(field.getName()), (Long) value);
                } else if (TextUtils.equals(field.getType().getSimpleName(), "Float")) {
                    cv.put(String.valueOf(field.getName()), (Float) value);
                } else if (TextUtils.equals(field.getType().getSimpleName(), "Double")) {
                    cv.put(String.valueOf(field.getName()), (Double) value);
                } else if (TextUtils.equals(field.getType().getSimpleName(), "Boolean")) {
                    cv.put(String.valueOf(field.getName()), (Boolean) value);
                } else {
                    cv.put(String.valueOf(field.getName()), AppUtil.getJson(value));
                }
            }
        } catch (Exception e) {
            Logger.e(LOG_TAG, "exception creating content value " + e.getCause());
            throw new RuntimeException(e);
        }
        return cv;
    }

    public static <T> T getFromContentValue(Cursor cursor, Class<T> clazz) {
        ContentValues contentValue = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor, contentValue);
        T object = null;
        try {
            Constructor<T> constructor = clazz.getConstructor();
            object = constructor.newInstance();
            Set<String> fields = contentValue.keySet();
            for (String field : fields) {
                String name = field;
                String methodName = SET_STRING + Character.toUpperCase(name.charAt(0)) + name.substring(1);
                Method method = clazz.getMethod(methodName, clazz.getField(field).getType());
                if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "Integer")) {
                    method.invoke(object, contentValue.getAsInteger(name));
                } else if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "Float")) {
                    method.invoke(object, contentValue.getAsFloat(name));
                } else if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "String")) {
                    method.invoke(object, contentValue.getAsString(name));
                } else if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "Long")) {
                    method.invoke(object, contentValue.getAsLong(name));
                } else if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "Double")) {
                    method.invoke(object, contentValue.getAsDouble(name));
                } else if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "Boolean")) {
                    Integer i = contentValue.getAsInteger(name);
                    if (i == null) {
                        method.invoke(object, false);
                    } else {
                        method.invoke(object, i.equals(1));
                    }
                } else {
                    String json = (String) contentValue.get(name);
                    if (TextUtils.equals(clazz.getField(field).getType().getSimpleName(), "List")) {
                        Type type = clazz.getField(field).getGenericType();
                        method.invoke(object, new Gson().fromJson(json, type));
                    } else {
                        method.invoke(object, AppUtil.parseJson((String) contentValue.get(name), clazz.getField(field).getType()));
                    }
                }
            }
        } catch (Exception e) {
            Logger.e(LOG_TAG, "exception when creating object from content" + e.getCause());
            throw new RuntimeException(e);
        }
        return object;
    }

    public static String getCreateTableStatement(Class<?> clazz) {
        String tableName = clazz.getSimpleName();
        Field[] fields = clazz.getDeclaredFields();
        String idType = "";
        List<TableFieldItem> fieldNames = new ArrayList<TableFieldItem>();
        for (Field field : fields) {
            TableFieldItem item = new TableFieldItem();
            item.fieldName = field.getName();
            item.isPrimitive = field.getType().isPrimitive();
            item.variableType = varTypeMapping.get(field.getType().getSimpleName()) != null ? varTypeMapping.get(field.getType().getSimpleName()) : "TEXT";
            item.isUnique = field.getAnnotation(Unique.class) != null;
            item.isTransient = Modifier.isTransient(field.getModifiers());
            fieldNames.add(item);
            if (TextUtils.equals(field.getName(), "_id")) {
                idType = field.getType().getSimpleName();
            }
        }
        // TODO : change query string to StringBuilder Type

        String query = "Create table " + tableName + " (" + BaseColumns._ID + " ";
        query = query + idType + " PRIMARY KEY ";
        /*
         * if (TextUtils.equals(idType, "Integer")) { query = query +
         * "AUTOINCREMENT NOT NULL"; }
         */
        query = query + " , ";
        if (fieldNames.size() == 1) {
            query = query.replace(" ,", "");
        }
        query = addFieldsInTable(query, fieldNames);
        if(query.endsWith(" , ")){
            query =  query.substring(0, query.length()-2);
        }
        query = query + " ) ;";
        Logger.i(LOG_TAG, "check create table statement " + query);
        return query;
    }

    private static String addFieldsInTable(String string, List<TableFieldItem> fieldNames) {
        for (int i = 0; i < fieldNames.size(); i++) {
            TableFieldItem item = fieldNames.get(i);
            if (!item.isTransient && !TextUtils.equals(item.fieldName, "_id")) {
                string = string + " " + item.fieldName + " " + item.variableType + " " + String.valueOf(item.isUnique ? "UNIQUE" : "") + String.valueOf(i <= (fieldNames.size() - 2) ? " , " : "  ");
            }
        }
        return string;
    }


    private static class TableFieldItem {
        String fieldName;
        boolean isUnique;
        boolean isPrimitive;
        String variableType;
        boolean isTransient;
    }

    public static <T> List<T> getListFromCursor(Cursor cursor, Class<T> class1) {
        List<T> list = new ArrayList<T>();
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            while (true) {
                T t = getFromContentValue(cursor, class1);
                list.add(t);
                if (cursor.isLast())
                    break;
                cursor.moveToNext();
            }
        }
        return list;
    }

}

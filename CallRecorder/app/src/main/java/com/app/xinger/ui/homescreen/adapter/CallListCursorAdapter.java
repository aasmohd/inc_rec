package com.app.xinger.ui.homescreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.recordingscreen.RecordingActivity;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;
import com.squareup.picasso.Picasso;

public class CallListCursorAdapter extends RecyclerViewCursorAdapter<CallListCursorAdapter.ViewHolder> {
    private final LayoutInflater layoutInflater;
    Context context;
    public CallListCursorAdapter(final Context context) {
        super();
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.item_call_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final Cursor cursor, int position) {
       // holder.bindData(cursor);
        final RecordedInfoModel record= DBUtil.getFromContentValue(cursor, RecordedInfoModel.class);
        if (position!=0) {
            final RecordedInfoModel prevrecord= DBUtil.getFromContentValue(getItem(position-1), RecordedInfoModel.class);
            if (AppUtils.getDate(record.getTime(),
                    AppConstants.DATEFORMAT_dd_MMM_yyyy).equalsIgnoreCase(AppUtils.getDate(
                    prevrecord.getTime(),AppConstants.DATEFORMAT_dd_MMM_yyyy))) {
                holder.txt_date.setVisibility(View.GONE);
            }
            else {
                holder.txt_date.setVisibility(View.VISIBLE);
            }
        }
        else {
            holder.txt_date.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(record.getContactName())) {
            holder.txt_name.setText(record.getMobileNo().trim());
        }
        else {
            holder.txt_name.setText(record.getContactName().trim());
        }
        if (!record.getProfilePic().equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(record.getProfilePic())
                    .into(holder.img_user_pic);
        }
        else {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            if (!TextUtils.isEmpty(record.getContactName())) {
                holder.img_user_pic.setImageDrawable(TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)/* size in px */
                        .toUpperCase()
                        .endConfig()
                        .buildRound(record.getContactName().toUpperCase().charAt(0)+"",generator.getColor(record.getMobileNo().trim())));
            }
            else{
                holder.img_user_pic.setImageDrawable(TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)/* size in px */
                        .toUpperCase()
                        .endConfig()
                        .buildRound("N",generator.getColor(record.getMobileNo().trim())));
            }

        }
        Log.e("IsInComing>>",record.getIsInComing()+"");
        if (record.getIsInComing()==1){
            holder.txt_name.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.incall,0);
        }
        else {
            holder.txt_name.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.outcall,0);
        }

        if (record.getIsFavourite().equalsIgnoreCase("1")) {
            holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starfilled));
        }
        else {
            holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starstroke));
        }
        holder.img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (record.getIsFavourite().equalsIgnoreCase("1")) {
                    record.setIsFavourite("0");
                    holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starstroke));
                }
                else {
                    record.setIsFavourite("1");
                    holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starfilled));
                }
                RecordedInfoDao.updateFavourite(record);
            }
        });
        holder.txt_time.setText(AppUtils.getDate(record.getTime(),
                AppConstants.DATEFORMAT_HH_mm_a)+" | "+AppUtils.getAudioDuration(record.getFilePath()));
        holder.txt_date.setText(AppUtils.getDate(record.getTime(),
                AppConstants.DATEFORMAT_dd_MMM_yyyy));

        holder.img_sub_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyPopUpMenu().ShowPopUp(context,record,holder.img_sub_menu);
            }
        });
        holder.rl_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, RecordingActivity.class);
                in.putExtra(AppConstants.RECORD_DATA,AppUtils.getGsonInstance().toJson(record));
                context.startActivity(in);
            }
        });

        holder.rl_record.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
              //  updateCheckedState(holder,record,true);
                return false;
            }
        });
      //  updateCheckedState(holder,record,false);
    }

    private void updateCheckedState(ViewHolder holder, RecordedInfoModel item,boolean isChecked) {
        if (isChecked) {
            holder.rl_record.setBackgroundColor(0x99a6a6a6);
            holder.img_checked.setVisibility(View.VISIBLE);
            holder.img_user_pic.setVisibility(View.GONE);
        }
        else {
            holder.rl_record.setBackgroundColor(Color.WHITE);
            holder.img_checked.setVisibility(View.GONE);
            holder.img_user_pic.setVisibility(View.VISIBLE);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name,txt_time,txt_date;
        ImageView img_user_pic,img_favourite,img_sub_menu,img_checked;
        RelativeLayout rl_record;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_date= (TextView)itemView.findViewById(R.id.txt_date);
            txt_time= (TextView)itemView.findViewById(R.id.txt_time);
            img_user_pic= (ImageView) itemView.findViewById(R.id.img_user_pic);
            img_favourite= (ImageView) itemView.findViewById(R.id.img_favourite);
            img_sub_menu= (ImageView) itemView.findViewById(R.id.img_sub_menu);
            img_checked= (ImageView) itemView.findViewById(R.id.img_checked);
            rl_record= (RelativeLayout) itemView.findViewById(R.id.rl_record);

        }
    }
}
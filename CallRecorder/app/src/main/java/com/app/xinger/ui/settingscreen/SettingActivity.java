package com.app.xinger.ui.settingscreen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.ActivitySettingBinding;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.ui.excludedscreen.ExcludedContactActivity;
import com.app.xinger.ui.inapppurchase.InAppPurchaseActivity;
import com.app.xinger.utils.AppUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

public class SettingActivity extends BaseActivity implements View.OnClickListener{

    ActivitySettingBinding viewBinding;
    Context context;
    private InterstitialAd mInterstitialAd;
    private ArrayList<String> callLimit = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_setting);
        context = this;
        callLimit.add("10");
        callLimit.add("200");
        callLimit.add("300");
        callLimit.add("500");
        callLimit.add("1000 Pro");
        viewBinding.toolbar.setTitle(R.string.settings);
        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        viewBinding.switchCallRecorder.setOnClickListener(this);
        viewBinding.switchIncomingCall.setOnClickListener(this);
        viewBinding.switchOutgoingCall.setOnClickListener(this);
        viewBinding.txtAudioSource.setOnClickListener(this);
        viewBinding.txtNeverRecorded.setOnClickListener(this);
        viewBinding.txtRecordingLimit.setOnClickListener(this);

        viewBinding.switchCallRecorder.setChecked(AppUtils.getBooleanPrefValue(context,AppUtils.ENABLE_CALL_RECORDER));
        viewBinding.switchOutgoingCall.setChecked(AppUtils.getBooleanPrefValue(context,AppUtils.ENABLE_OUTGOING_CALL));
        viewBinding.switchIncomingCall.setChecked(AppUtils.getBooleanPrefValue(context,AppUtils.ENABLE_INCOMING_CALLS));
        viewBinding.switchShowNotification.setChecked(AppUtils.getBooleanPrefValue(context,AppUtils.SHOW_NOTIFICATION));

        viewBinding.txtFileLocation.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.file_location)
                +"</font><br/> <font color=#a6a6a6>"+ Environment.getExternalStorageDirectory().getPath()+
                AppConstants.STORAGE_DIR+"</font>"));

        viewBinding.txtAudioSource.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.audio_source)
                +"</font><br/> <font color=#a6a6a6>"+AppUtils.getStringPrefValue(context,AppUtils.AUDIO_SOURCE_NAME)+"</font>"));

        if (getSelectedLimit()==callLimit.size()-1) {
            viewBinding.txtRecordingLimit.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.total_call_record)
                    +"</font><br/> <font color=#a6a6a6>"+AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+" Pro</font>"));
        }
        else {
            viewBinding.txtRecordingLimit.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.total_call_record)
                    +"</font><br/> <font color=#a6a6a6>"+AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+"</font>"));
        }
        AdRequest adRequest = new AdRequest.Builder().build();
        viewBinding.adView.loadAd(adRequest);
        if (AppUtils.isShowFullAdd()) {
            mInterstitialAd = new InterstitialAd(SettingActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }

        viewBinding.switchCallRecorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_CALL_RECORDER,
                        viewBinding.switchCallRecorder.isChecked()?true:false);
                if (viewBinding.switchCallRecorder.isChecked()){
                    viewBinding.switchOutgoingCall.setChecked(true);
                    viewBinding.switchIncomingCall.setChecked(true);
                }
            }
        });

        viewBinding.switchOutgoingCall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_OUTGOING_CALL,
                        viewBinding.switchOutgoingCall.isChecked()?true:false);
            }
        });

        viewBinding.switchIncomingCall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_INCOMING_CALLS,
                        viewBinding.switchIncomingCall.isChecked()?true:false);
            }
        });

        viewBinding.switchShowNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppUtils.setBooleanPrefValue(context,AppUtils.SHOW_NOTIFICATION,
                        viewBinding.switchShowNotification.isChecked()?true:false);
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.switch_call_recorder:
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_CALL_RECORDER,
                        viewBinding.switchCallRecorder.isChecked()?true:false);
                if (viewBinding.switchCallRecorder.isChecked()){
                    viewBinding.switchOutgoingCall.setVisibility(View.VISIBLE);
                    viewBinding.switchIncomingCall.setVisibility(View.VISIBLE);
                }
                else {
                    viewBinding.switchOutgoingCall.setVisibility(View.GONE);
                    viewBinding.switchIncomingCall.setVisibility(View.GONE);
                }
                break;

            case R.id.switch_outgoing_call:
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_OUTGOING_CALL,
                        viewBinding.switchOutgoingCall.isChecked()?true:false);
                if (!viewBinding.switchOutgoingCall.isChecked()&& !viewBinding.switchIncomingCall.isChecked()){
                    viewBinding.switchCallRecorder.setChecked(false);
                }
                else {
                    viewBinding.switchCallRecorder.setChecked(true);
                }
                break;

            case R.id.switch_incoming_call:
                AppUtils.setBooleanPrefValue(context,AppUtils.ENABLE_INCOMING_CALLS,
                        viewBinding.switchIncomingCall.isChecked()?true:false);
                if (!viewBinding.switchOutgoingCall.isChecked()&& !viewBinding.switchIncomingCall.isChecked()){
                    viewBinding.switchCallRecorder.setChecked(false);
                }
                else {
                    viewBinding.switchCallRecorder.setChecked(true);
                }
                break;

            case R.id.switch_show_notification:
                AppUtils.setBooleanPrefValue(context,AppUtils.SHOW_NOTIFICATION,
                        viewBinding.switchShowNotification.isChecked()?true:false);
                break;

            case R.id.txt_audio_source:
                showAlertDialog(getString(R.string.audio_source),AppUtils.
                        getIntegerPrefValue(context,AppUtils.AUDIO_SOURCE)== MediaRecorder.AudioSource.VOICE_CALL?
                1:0);
                break;

            case R.id.txt_file_type:
                break;

            case R.id.txt_never_recorded:
                Intent in = new Intent(context, ExcludedContactActivity.class);
                startActivity(in);
                break;

            case R.id.txt_recording_limit:
                if (!AppUtils.getBooleanPrefValue(context,AppConstants.PREF_KEY_PRO_VERSION)) {
                    showCallLimitDialog();
                }
                else {
                    AppUtils.showCustomToast(context,getString(R.string.msg_no_limit_on_calls));
                }
                break;

            case R.id.txt_file_location:
                break;

        }
    }


    public void showAlertDialog(final String title, int selectOption) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(SettingActivity.this);
        builderSingle.setTitle(title);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SettingActivity.this, android.R.layout.select_dialog_singlechoice,getResources().getStringArray(R.array.audio_source_name));
        builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.setSingleChoiceItems(getResources().getStringArray(R.array.audio_source_name), selectOption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, final int i) {
                dialogInterface.cancel();
                AlertDialog.Builder builderInner = new AlertDialog.Builder(SettingActivity.this);
                builderInner.setMessage(arrayAdapter.getItem(i));
                builderInner.setTitle("Your Selected Item is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {

                        if (title.contains(getString(R.string.audio_source))) {

                            AppUtils.setIntegerPrefValue(context,AppUtils.AUDIO_SOURCE,i==0?
                                    MediaRecorder.AudioSource.MIC:MediaRecorder.AudioSource.VOICE_CALL);
                            AppUtils.setStringPrefValue(context,AppUtils.AUDIO_SOURCE_NAME,arrayAdapter.getItem(i));

                        }
                        else {
                            AppUtils.setIntegerPrefValue(context,AppUtils.AUDIO_SOURCE,i==0?
                                    MediaRecorder.AudioSource.MIC:MediaRecorder.AudioSource.VOICE_CALL);
                            AppUtils.setStringPrefValue(context,AppUtils.AUDIO_SOURCE_NAME,arrayAdapter.getItem(i));
                        }
                        viewBinding.txtAudioSource.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.audio_source)
                                +"</font><br/> <font color=#a6a6a6>"+AppUtils.getStringPrefValue(context,AppUtils.AUDIO_SOURCE_NAME)+"</font>"));
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();
    }


    public void showCallLimitDialog() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(SettingActivity.this);
        builderSingle.setTitle(R.string.call_limit);
        builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.setSingleChoiceItems(getResources().getStringArray(R.array.audio_call_limit), getSelectedLimit(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, final int i) {
                dialogInterface.cancel();

                if (i==callLimit.size()-1){
                     AlertDialog.Builder builderInner = new AlertDialog.Builder(SettingActivity.this);
                    builderInner.setMessage(callLimit.get(i));
                    builderInner.setTitle(R.string.msg_pro_version);
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            Intent in = new Intent(context, InAppPurchaseActivity.class);
                            startActivity(in);
                           /* viewBinding.txtRecordingLimit.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.total_call_record)
                                    +"</font><br/> <font color=#a6a6a6>"+AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+" Pro </font>"));*/
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
                else {
                    AppUtils.setIntegerPrefValue(context,AppUtils.CALL_LIMIT,Integer.parseInt(callLimit.get(i)));
                    viewBinding.txtRecordingLimit.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.total_call_record)
                            +"</font><br/> <font color=#a6a6a6>"+AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+"</font>"));
                }


            }
        });
        builderSingle.show();
    }

    public int getSelectedLimit() {
        if (AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)==1000)
            return callLimit.size()-1;
        for (int i = 0; i < callLimit.size(); i++) {
            if (callLimit.get(i).equalsIgnoreCase(AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+"")){
                return i;
            }
        }
        return 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppUtils.getBooleanPrefValue(context,AppConstants.PREF_KEY_PRO_VERSION)){
            viewBinding.txtRecordingLimit.setText(Html.fromHtml("<font color=#000000>"+getString(R.string.total_call_record)
                    +"</font><br/> <font color=#a6a6a6>"+AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)+" Pro </font>"));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

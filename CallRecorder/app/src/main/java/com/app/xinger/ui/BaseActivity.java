package com.app.xinger.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.xinger.utils.AppUtils;
import com.app.xinger.utils.MaterialProgressDialog;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends AppCompatActivity {
    private MaterialProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * This method is used to show/hide progress dialog with  message
     */
    public void showProgressDialog(boolean isShow) {
        if (mProgressDialog == null) {
            mProgressDialog = AppUtils.getProgressDialogInstance(this);
        }
        try {
            if (isShow)
                mProgressDialog.show();
            else {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

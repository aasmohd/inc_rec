package com.app.xinger.ui.homescreen;

import android.Manifest;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.ActivityHomeScreenBinding;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.ui.homescreen.listener.CallDataLoader;
import com.app.xinger.ui.homescreen.listener.PermissionListener;
import com.app.xinger.utils.AppUtils;
import com.app.xinger.utils.PermissionChecker;
import com.google.android.gms.ads.AdRequest;

public class HomeScreenActivity extends BaseActivity implements DrawerFragment.OnFragmentInteractionListener,PermissionListener{

    private HomeAdapter fragmentAdapter;
    ActivityHomeScreenBinding viewBinding;
    Context context;
    boolean showAlert= true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_home_screen);
        context = this;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(AppConstants.NOTIFICATION_ID);
        setSupportActionBar(viewBinding.mainContent.toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        // Set up the ViewPager with the sections adapter.
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, viewBinding.drawerLayout, viewBinding.mainContent.toolbar,R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        viewBinding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        setFragments();

        if (!PermissionChecker.checkPermission(HomeScreenActivity.this,new String[]{
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        })){
            //AppUtils.ShowAlert(HomeScreenActivity.this);
            PermissionChecker.requestPermission(context,new String[]{
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
            });
        }

        AdRequest adRequest = new AdRequest.Builder().build();
        viewBinding.mainContent.adView.loadAd(adRequest);

        viewBinding.mainContent.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab selectedTab) {
                LinearLayout tabLayout = (LinearLayout)((ViewGroup)viewBinding.mainContent.tabLayout.getChildAt(0)).getChildAt(selectedTab.getPosition());
                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);
            }
            @Override
            public void onTabUnselected(TabLayout.Tab selectedTab) {
                LinearLayout tabLayout = (LinearLayout)((ViewGroup)viewBinding.mainContent.tabLayout.getChildAt(0)).getChildAt(selectedTab.getPosition());
                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                tabTextView.setTypeface( Typeface.createFromAsset(
                        getAssets(),
                        "SFUIText-Regular.ttf"));
            }

            @Override
            public void onTabReselected(TabLayout.Tab selectedTab) {
                LinearLayout tabLayout = (LinearLayout)((ViewGroup)viewBinding.mainContent.tabLayout.getChildAt(0)).getChildAt(selectedTab.getPosition());
                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        if (showAlert){
            Bundle b = getIntent().getExtras();
            if (b!=null) {
                if (b.containsKey(AppConstants.RECORD_DATA)){
                    AppUtils.ShowDeleteAlert(context,AppUtils.getGsonInstance().fromJson(
                            getIntent().getExtras().getString(AppConstants.RECORD_DATA),RecordedInfoModel.class),false);
                    showAlert=false;
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                ((CallDataLoader)fragmentAdapter.getItem(viewBinding.mainContent.viewPager.getCurrentItem()))
                        .onSearchData(viewBinding.mainContent.viewPager.getCurrentItem(),newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_hindi) {
            AppUtils.changeLanguage(context,"hi");
            return true;
        }
        if (id == R.id.action_english) {
            AppUtils.changeLanguage(context,"en");
            return true;
        }
        if (id == R.id.action_french) {
            AppUtils.changeLanguage(context,"fr");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /*add all membership plan fragment*/
    public void setFragments() {
        fragmentAdapter = new HomeAdapter(getSupportFragmentManager());
        fragmentAdapter.addFragment(getString(R.string.tab_all),new CallListFragment());
        fragmentAdapter.addFragment(getString(R.string.tab_outgoing),new CallListFragment());
        fragmentAdapter.addFragment(getString(R.string.tab_incoming),new CallListFragment());
        fragmentAdapter.addFragment(getString(R.string.tab_important),new CallListFragment());
        viewBinding.mainContent.viewPager.setAdapter(fragmentAdapter);
        viewBinding.mainContent.viewPager.setOffscreenPageLimit(4);
        viewBinding.mainContent.tabLayout.setupWithViewPager(viewBinding.mainContent.viewPager);

        viewBinding.mainContent.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                if (CallListFragment.actionMode!=null)
                    CallListFragment.actionMode.finish();
                ((CallDataLoader)fragmentAdapter.getItem(position)).loadData(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        LinearLayout tabLayout = (LinearLayout)((ViewGroup)viewBinding.mainContent.tabLayout.getChildAt(0)).getChildAt(0);
        TextView tabTextView = (TextView) tabLayout.getChildAt(1);
        tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);

    }

    @Override
    public void onFragmentInteraction() {
        viewBinding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onPressOk() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==PermissionChecker.REQUEST_CODE) {
            if (!PermissionChecker.checkPermission(HomeScreenActivity.this,new String[]{
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
            })) {
                AppUtils.showCustomToast(context,getString(R.string.msg_permission));
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        RecordedInfoDao.updateAllRecord();
    }
}

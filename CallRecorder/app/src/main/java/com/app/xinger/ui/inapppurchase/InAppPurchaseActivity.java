package com.app.xinger.ui.inapppurchase;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.ActivityInAppPurchaseBinding;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.utils.AppUtils;

public class InAppPurchaseActivity extends BaseActivity implements BillingProcessor.IBillingHandler{

    ActivityInAppPurchaseBinding viewBinding;
    private BillingProcessor bp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_in_app_purchase);
        // Create and initialize BillingManager which talks to BillingLibrary
        viewBinding.toolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));

        bp = new BillingProcessor(InAppPurchaseActivity.this, AppConstants.LICENSE_KEY, InAppPurchaseActivity.this);
        bp.initialize();
        viewBinding.btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bp.purchase(InAppPurchaseActivity.this, AppConstants.PRODUCT_ID);
            }
        });
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        AppUtils.showCustomToast(InAppPurchaseActivity.this,getString(R.string.msg_pro_successfull));
        AppUtils.setIntegerPrefValue(InAppPurchaseActivity.this,AppUtils.CALL_LIMIT,1000);
        AppUtils.setBooleanPrefValue(InAppPurchaseActivity.this,AppConstants.PREF_KEY_PRO_VERSION,true);
        AppUtils.ShowDropBoxLoginAlert(InAppPurchaseActivity.this);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }


    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

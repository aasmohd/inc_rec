package com.app.xinger.ui.excludedscreen;

import com.app.xinger.dbhelper.DBClass;

import java.io.Serializable;

/**
 * Created by shubh on 31-Aug-17.
 */

public class ExcludedContactModel  implements DBClass, Serializable {
    public Integer _id;
    public String mobileNo,contactName;

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
}

package com.app.xinger.ui.excludedscreen;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.xinger.R;
import com.app.xinger.ui.homescreen.adapter.RecyclerViewCursorAdapter;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;

public class ExcludedContactCursorAdapter extends RecyclerViewCursorAdapter<ExcludedContactCursorAdapter.ViewHolder> {
    private final LayoutInflater layoutInflater;
    Context context;
    public ExcludedContactCursorAdapter(final Context context) {
        super();
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.item_excluded_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final Cursor cursor, int position) {
       // holder.bindData(cursor);
        final ExcludedContactModel record= DBUtil.getFromContentValue(cursor, ExcludedContactModel.class);



        if (TextUtils.isEmpty(record.getContactName())) {
            holder.txt_name.setText(record.getMobileNo());
            holder.txt_number.setVisibility(View.GONE);
        }
        else {
            holder.txt_number.setVisibility(View.VISIBLE);
            holder.txt_number.setText(record.getMobileNo());
            holder.txt_name.setText(record.getContactName());
        }

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.ShowExcludedDeleteAlert(context,record.getMobileNo());

            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name,txt_number;
        ImageView img_delete;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_name= (TextView)itemView.findViewById(R.id.txt_name);
            txt_number= (TextView)itemView.findViewById(R.id.txt_number);
            img_delete= (ImageView) itemView.findViewById(R.id.img_delete);


        }
    }
}
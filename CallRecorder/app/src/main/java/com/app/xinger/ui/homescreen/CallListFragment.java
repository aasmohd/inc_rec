package com.app.xinger.ui.homescreen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.FragmentCallListBinding;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.homescreen.adapter.CallListViewCursorAdapter;
import com.app.xinger.ui.homescreen.listener.CallDataLoader;
import com.app.xinger.ui.recordingscreen.RecordingActivity;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;

/**
 * A placeholder fragment containing a simple view.
 */
public class CallListFragment extends Fragment  implements LoaderManager.LoaderCallbacks<Cursor>,CallDataLoader {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    FragmentCallListBinding viewBinding;
    Context context;
    CallListViewCursorAdapter adapter;
    boolean isMultiSelect = false;
    ListView listview;
    static ActionMode actionMode;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_call_list, container, false);

        return viewBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        /*viewBinding.recyclerView.setHasFixedSize(true);
        viewBinding.recyclerView.setLayoutManager(mLinearLayoutManager);*/
        adapter = new CallListViewCursorAdapter(context);
        viewBinding.listView.setAdapter(adapter);
        viewBinding.txtTotalCalls.setVisibility(View.GONE);
        loadData(getArguments().getInt("pos", 0));
        viewBinding.listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        viewBinding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent in = new Intent(getActivity(), RecordingActivity.class);
                in.putExtra(AppConstants.RECORD_DATA, AppUtils.getGsonInstance().toJson(DBUtil.
                        getFromContentValue((Cursor)adapter.getItem(position), RecordedInfoModel.class)));
                startActivity(in);

            }
        });

        viewBinding.listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                int recordId = adapter.getCursor().getInt(adapter.getCursor().getColumnIndex("_id"));
                if (adapter.isSelected(recordId)){
                    for (int i = 0; i<adapter.getMultiselect_list().size(); i++) {
                        if (adapter.getMultiselect_list().get(i)==recordId){
                            adapter.getMultiselect_list().remove(i);
                            break;
                        }
                    }
                }
                else {
                    adapter.getMultiselect_list().add(recordId);
                }
                multi_select(mode);
                adapter.notifyDataSetChanged();
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode .getMenuInflater().inflate(R.menu.menu_multi_select, menu);
                actionMode = mode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        //mode.finish();
                        ShowDeleteAlert();
                        // Close CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode=null;
                adapter.removeAllSelection();
            }
        });
    }

    public void ShowDeleteAlert() {

        String message;
        if (adapter.getMultiselect_list().size()>1){
            message = "Are you sure you want to delete "+adapter.getMultiselect_list().size()+" recordings?";
        }
        else
            message = "Are you sure you want to delete "+adapter.getMultiselect_list().size()+" recording?";

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        for (int i = 0; i < adapter.getMultiselect_list().size(); i++) {
                            RecordedInfoDao.deleteSelectedRecord(adapter.getMultiselect_list().get(i));
                        }

                        actionMode.finish();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        actionMode.finish();
                    }
                })
                .show();
    }
    public void multi_select(ActionMode mActionMode) {
            if (adapter.getMultiselect_list().size() > 0)
                mActionMode.setTitle("" + adapter.getMultiselect_list().size()+" Selected");
            else
                mActionMode.setTitle("");
           adapter.notifyDataSetChanged();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case AppConstants.ALL :
                if (args!=null){
                    return RecordedInfoDao.getCursorLoader(args.getString(AppConstants.SearchText));
                }
                else {
                    return RecordedInfoDao.getCursorLoader("");
                }
            case AppConstants.OUTGOING :
                if (args!=null){
                    return RecordedInfoDao.getFilteredCursorLoader(0,args.getString(AppConstants.SearchText));
                }
                else {
                    return RecordedInfoDao.getFilteredCursorLoader(0,"");
                }
            case AppConstants.INCOMING:
                if (args!=null){
                    return RecordedInfoDao.getFilteredCursorLoader(1,args.getString(AppConstants.SearchText));
                }
                else {
                    return RecordedInfoDao.getFilteredCursorLoader(1,"");
                }
            case AppConstants.IMPORTANT:
                if (args!=null){
                    return RecordedInfoDao.getFavouriteLoader(args.getString(AppConstants.SearchText));
                }
                else {
                    return RecordedInfoDao.getFavouriteLoader("");
                }
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            adapter.swapCursor(data);
            int count = data.getCount();
            if (count>0){
                viewBinding.txtNoRecording.setVisibility(View.GONE);
                viewBinding.txtTotalCalls.setVisibility(View.VISIBLE);
                viewBinding.txtTotalCalls.setText(count==1?"Total Call : "+count:"Total Calls : "+count);
            }
            else{
                viewBinding.txtNoRecording.setVisibility(View.VISIBLE);
                viewBinding.txtTotalCalls.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void loadData(final int recordType) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportLoaderManager().initLoader(recordType, null, CallListFragment.this);
            }
        },400);
    }

    @Override
    public void onSearchData(int recordType, String searchText) {
        Bundle b = new Bundle();
        b.putString(AppConstants.SearchText,searchText);
        getActivity().getSupportLoaderManager().restartLoader(recordType,b, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

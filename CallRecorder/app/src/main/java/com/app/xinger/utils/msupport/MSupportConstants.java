package com.app.xinger.utils.msupport;

import android.Manifest;

/**
 * Created by JTI on 28 Feb 2017.
 */
public class MSupportConstants {
    /**
     * This Constants for SDK needed to be compare with
     */
    static final int SDK_VERSION = 23;
    public static final int CALL_PHONE_REQUEST_CODE = 123;
    public static final int READ_CONTACTS_REQUEST_CODE = 1231;
    public static final int CALENDER_REQUEST_CODE = 1256;
    public static final int LOCATION_REQUEST_CODE = 1234;
    public static final int REQUEST_STORAGE_READ_WRITE = 2;
    public static final int RECORD_AUDIO_REQUEST_CODE = 1212;

    /**
     * Marshmallow permission list constants
     */
    public static final String CALL_PHONE = Manifest.permission.CALL_PHONE;
    public static final String CALENDER = Manifest.permission.WRITE_CALENDAR;
    public static final String GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS;

    public static final String ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;

    /**
     * Get combined rationale message for multiple permission
     *
     * @param permission Permission
     * @return Message for permission
     */
    static String getPermissionRationaleMessage(String permission) {
        switch (permission) {
            case GET_ACCOUNTS:
                return "Contacts";
            case CALENDER:
                return "Calender";
            default:
                return "PERMISSION";
        }
    }


}

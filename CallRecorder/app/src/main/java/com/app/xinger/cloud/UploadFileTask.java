package com.app.xinger.cloud;

/**
 * Created by shubh on 27-Nov-17.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;

import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.ui.cluodStorage.CloudListener;
import com.app.xinger.utils.AppUtils;
import com.dropbox.core.DbxException;
import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.WriteMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static android.content.Context.MODE_PRIVATE;

/**
 * Async task to upload a file to a directory
 */
public class UploadFileTask extends AsyncTask<String, Void, FileMetadata> {

    private final Context mContext;
    private DbxClientV2 mDbxClient;
    private Exception mException;
    Boolean showProgress;
    public interface Callback {
        void onUploadComplete(FileMetadata result);
        void onError(Exception e);
    }

    public UploadFileTask(Context context, DbxClientV2 dbxClient,boolean showProgress) {


        mContext = context;
        this.showProgress= showProgress;
        if (dbxClient==null){
            if (AppUtils.hasToken(mContext)) {
                initDropBox();
                mDbxClient = DropboxClientFactory.getClient();
                execute();
            }
        }
        else {
            mDbxClient = dbxClient;
            execute();
        }
    }

    @Override
    protected void onPostExecute(FileMetadata result) {
        super.onPostExecute(result);
        if (showProgress)
            ((CloudListener)mContext).hideProgress();
    }

    @Override
    protected FileMetadata doInBackground(String... params) {

        Cursor cursor = RecordedInfoDao.getAllRecordFile(0);

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        uploadOnDropBox(cursor.getString(cursor.getColumnIndex("_id")),
                                cursor.getString(cursor.getColumnIndex("filePath")));
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public void uploadOnDropBox(String recordId,String filePath) {
        File file = new File(filePath);
        if (file != null) {
            String remoteFolderPath ="";

            // Note - this is not ensuring the name is a valid dropbox file name
            String remoteFileName = file.getName();
            try {
                FileInputStream inputStream = new FileInputStream(file);
                mDbxClient.files().uploadBuilder(remoteFolderPath + "/" + remoteFileName)
                        .withMode(WriteMode.OVERWRITE)
                        .uploadAndFinish(inputStream);

                RecordedInfoDao.updateFileStatus(filePath,1,recordId);
            } catch (DbxException | IOException e) {
                mException = e;
            }
        }
    }

    public void initDropBox() {

        SharedPreferences prefs = mContext.getSharedPreferences(AppConstants.ACCOUNT_PREFS_NAME, MODE_PRIVATE);
        String accessToken = prefs.getString(AppConstants.ACCESS_TOKEN , null);
        if (accessToken == null) {
            accessToken = Auth.getOAuth2Token();
            if (accessToken != null) {
                prefs.edit().putString(AppConstants.ACCESS_TOKEN, accessToken).apply();
                DropboxClientFactory.init(accessToken);
            }
        } else {
            DropboxClientFactory.init(accessToken);
        }

    }
}
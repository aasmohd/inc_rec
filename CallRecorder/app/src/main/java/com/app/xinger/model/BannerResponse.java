package com.app.xinger.model;

import java.util.ArrayList;

/**
 * Created by shubh on 27-Nov-17.
 */

public class BannerResponse {

    public String status,messages;
    public Result result;

    public class  Result {
        public ArrayList<BannerModel> banners;

    }
}

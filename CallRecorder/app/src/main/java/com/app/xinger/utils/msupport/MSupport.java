
package com.app.xinger.utils.msupport;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.app.xinger.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to handle M support logic
 * Created by JTI on 28 Feb 2017.
 */

public class MSupport {

    /**
     * @return true in case of M Device,
     * false in case of below M devices
     */
    private static boolean isMSupportDevice(Context ctx) {
        return Build.VERSION.SDK_INT >= MSupportConstants.SDK_VERSION;
    }


    /**
     * Method to check single permission with rationale
     *
     * @param mActivity   Calling activity context
     * @param fragment    Calling fragment instance
     * @param permission  Permission to check
     * @param requestCode request code
     * @return true in case of permission is granted or pre marshmallow
     * false in case of permission is not granted
     * in case of false we have to request that permission
     */

    @TargetApi(23)
    public static boolean checkPermissionWithRationale(final Activity mActivity, final Fragment fragment,
                                                       final String permission, final int requestCode) {

        if (MSupport.isMSupportDevice(mActivity)) {

            final List<String> permissions = new ArrayList<>();
            int hasPermission = mActivity.checkSelfPermission(permission);

            if (hasPermission != PackageManager.PERMISSION_GRANTED)
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {

                    String msg = "";
                    if (requestCode == MSupportConstants.CALL_PHONE_REQUEST_CODE) {
                        msg = mActivity.getString(R.string.call_phone_permission_rationale);
                    } else if (requestCode == MSupportConstants.REQUEST_STORAGE_READ_WRITE) {
                        msg = mActivity.getString(R.string.storage_permission_rationale);
                    } else if (requestCode == MSupportConstants.READ_CONTACTS_REQUEST_CODE) {
                        msg = mActivity.getString(R.string.read_contact_permission);
                    } else if (requestCode == MSupportConstants.RECORD_AUDIO_REQUEST_CODE) {
                        msg = mActivity.getString(R.string.record_permission);
                    }

                    showMessageOKCancel(mActivity, msg,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    permissions.add(permission);
                                    requestPermissions(permissions, fragment, mActivity, requestCode);
                                }
                            });
                    return false;

                } else {
                    permissions.add(permission);
                    requestPermissions(permissions, fragment, mActivity, requestCode);
                    return false;
                }
            else
                return true;

        }
        return true;
    }

    /**
     * Check permission require rationale or not
     *
     * @param mActivity       Activity context
     * @param permission      Permission to check
     * @param permissionsList Add in list if rationale required
     * @return Flag if rationale is required
     */
    @TargetApi(Build.VERSION_CODES.M)
    private static boolean addPermission(final Activity mActivity, String permission, List<String> permissionsList) {
        if (mActivity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (mActivity.shouldShowRequestPermissionRationale(permission))
                return true;
        }
        return false;
    }

    /**
     * Method to check single permission with rationale
     *
     * @param mActivity   Calling activity context
     * @param permissionsList2  Permission list to check
     * @param requestCode request code
     * @return true in case of permission is granted or pre marshmallow
     * false in case of permission is not granted
     * in case of false we have to request that permission
     */
    public static boolean checkMultiplePermission(final Activity mActivity, String[] permissionsList2, final int requestCode) {
        if (MSupport.isMSupportDevice(mActivity)) {

            List<String> permissionsNeeded = new ArrayList<>();
            final List<String> permissionsList = new ArrayList<>();

            for (String permission : permissionsList2) {
                if (addPermission(mActivity, permission, permissionsList)) {
                    permissionsNeeded.add(MSupportConstants.getPermissionRationaleMessage(permission));
                }
            }

            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale case
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(mActivity, message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(permissionsList, null, mActivity,
                                            requestCode);
                                }
                            });
                    return false;
                }
                requestPermissions(permissionsList, null, mActivity,
                        requestCode);
                return false;
            } else
                return true;
        } else
            return true;
    }

    /**
     * Method to request permission for activity or fragment
     *
     * @param permissions Permissions to ask for
     * @param fragment    Fragment instance if fragment is requesting a permission
     * @param mActivity   Activity instance if activity is requesting a permission
     * @param requestCode Request code for asked permission
     * @return Flag if permission is already allowed
     */
    @TargetApi(23)
    private static boolean requestPermissions(List<String> permissions, Fragment fragment,
                                              Activity mActivity, int requestCode) {
        if (!permissions.isEmpty()) {
            if (fragment != null)
                fragment.requestPermissions(permissions.toArray(new String[permissions.size()]),
                        requestCode);
            else
                mActivity.requestPermissions(permissions.toArray(new String[permissions.size()]),
                        requestCode);
            return false;
        } else
            return true;
    }

    /**
     * Method to show rationale message in a dialog
     *
     * @param activity   Activity object
     * @param message    Message to show
     * @param okListener Callback listener
     */
    private static void showMessageOKCancel(Activity activity, String message
            , DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(activity.getString(R.string.ok), okListener)
                .create()
                .show();
    }


}
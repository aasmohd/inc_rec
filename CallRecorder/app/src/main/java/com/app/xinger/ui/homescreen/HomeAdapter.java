package com.app.xinger.ui.homescreen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by shubh on 21-Aug-17.
 */

public class HomeAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragmentList = new ArrayList<>();
    private ArrayList<String> fragmentName = new ArrayList<>();

    public HomeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Fragment fragment = fragmentList.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt("pos",position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return fragmentList.size();
    }


    /**
     * Add Fragment object and fragment name.
     * @param name
     * @param fragment
     */
    public void addFragment(String name,Fragment fragment) {

        fragmentList.add(fragment);
        fragmentName.add(name);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentName.get(position);
    }

}

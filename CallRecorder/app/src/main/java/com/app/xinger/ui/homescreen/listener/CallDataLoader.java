package com.app.xinger.ui.homescreen.listener;

/**
 * Created by shubh on 30-Aug-17.
 */

public interface CallDataLoader {

    public void loadData(int recordType);
    public void onSearchData(int recordType,String searchText);

}
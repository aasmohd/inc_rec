package com.app.xinger.service;

import com.app.xinger.utils.AppUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Inflac on 02-11-2016.
 */
public class RetrofitInstance {

    public static  Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {

        if (retrofit==null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(UrlConfigurations.GetBaseURL())
                    .addConverterFactory(GsonConverterFactory.create(AppUtils.getGsonInstance()))
                    .client(client)
                    .build();
        }

        return retrofit;
    }



}

package com.app.xinger.constants;

/**
 * Created by shubh on 21-Aug-17.
 */

public class AppConstants {

    public final static String STORAGE_DIR = "/MyCallRecord/";
    public final static String FILEPROVIDER = "com.app.xinger.fileprovider";

    /*Pref keys*/
    public final static String PREF_KEY_PRO_VERSION= "pro_version";

    /*Dropbox keys details*/

    public final static String APP_KEY = "c1bhiy0soqnvcn5";
    public final static String APP_SECRET = "bzi50cd2qgb8odr";
    public final static String ACCESS_TOKEN = "access-token";
    public static final String ACCOUNT_PREFS_NAME = "call_recorder_prefs";
    public static final String ACCESS_KEY_NAME = "ACCESS_KEY";
    public static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";
    /*Play store link*/

    public static final String GOOGLE_PLAYSTORE_LINK= "https://play.google.com/store?hl=en";
    /*Date formate*/
    public static final String DATEFORMAT_HH_mm= "HH:mm";
    public static final String DATEFORMAT_HH_mm_a= "hh:mm a";
    public static final String DATEFORMAT_dd_MMM_yyyy = "dd MMM yyyy";
    public static final String DATEFORMAT_DD_MMM_YYYY_HH_MM_A= "dd MMM yyyy hh:mm a";
    public static final String RECORD_DATA= "recordData";

    public static String CONTENT_PROVIDER_AUTHORITY_NAME() {
        return "com.app.xinger.contentprovider";
    }

    /*Call record Type*/

    public static final int ALL = 0;
    public static final int OUTGOING= 1;
    public static final int INCOMING = 2;
    public static final int IMPORTANT= 3;

    /*Total Call record in our app*/
    public static final int TOTAL_CALL_COUNT= 20;


    public static final int NOTIFICATION_ID= 10101;



    public static final String SearchText= "search";


    /*Intent data keys*/
    public static final String BANNER_RESPONSE = "banner_response";

    /*Product id*/

    public static final String PRODUCT_ID= "com.app.xinger_payment";
    public static final String LICENSE_KEY= "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1KXyFSTurXEp6yAGMTDnusCvE30VsV+16cesudGnefPG4K1CwXzdvWJiHowD4IToYvRlgKf+KiQsOzIlMLwnP5luoD5PKMHon5IykHXOasNmx+jZ0z6PMQ1A8q4ukBdw0CWGMVq3ZXaiyFN/YKnLe6zDhHCm2XR3vw39afAywxNZYCPHINRBtwf0ob2RwVS+7yiy7tVvQSwGdR3IGob+amg+GNYyQDrdWExVWh54OnGmXFW7a1AYxRNMYNhEj7qTim2TDxEy2zprDOc9FlxYKCCijnFr2hWgAQ5vxxH+gR+HiqVz+jy5PAdIJTRIIvrbggNa2Oa27bMA45KTyLyOrQIDAQAB";

}

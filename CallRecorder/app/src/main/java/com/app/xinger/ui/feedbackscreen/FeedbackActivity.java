package com.app.xinger.ui.feedbackscreen;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.app.xinger.R;
import com.app.xinger.apiInterface.MyApiEndpointInterface;
import com.app.xinger.databinding.ActivityFeedbackBinding;
import com.app.xinger.service.RetrofitInstance;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.utils.AppUtils;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends BaseActivity {

    ActivityFeedbackBinding viewBinding;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding =(ActivityFeedbackBinding) DataBindingUtil.setContentView(this, R.layout.activity_feedback);
        viewBinding.toolbar.setTitle(R.string.label_app_feedback);
        viewBinding.toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));

        viewBinding.txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInputData();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    /*validate input data in feedback form*/
    public void checkInputData() {
        if (viewBinding.edtMailId.getText().toString().trim().equalsIgnoreCase("")){
            AppUtils.showCustomToast(FeedbackActivity.this, getString(com.app.xinger.R.string.msg_enter_email_id));
        }
        else  if (!AppUtils.isEmailValid(viewBinding.edtMailId.getText().toString().trim())){
            AppUtils.showCustomToast(FeedbackActivity.this, getString(com.app.xinger.R.string.msg_enter_valid_email_id));
        }
        else  if (viewBinding.edtFeedback.getText().toString().trim().equalsIgnoreCase("")){
            AppUtils.showCustomToast(FeedbackActivity.this, getString(com.app.xinger.R.string.msg_enter_feedback));
        }
        else {
            sendFeedback( viewBinding.edtMailId.getText().toString().trim(),
                    viewBinding.edtFeedback.getText().toString().trim());
        }
    }



    /**
     * Get all banner events.
     */
    public void sendFeedback(String email,String feedback) {
        if (AppUtils.isNetworkAvailable(FeedbackActivity.this)) {
            try {
                dialog = ProgressDialog.show(FeedbackActivity.this, "", getString(R.string.please_wait));
                JsonObject object = new JsonObject();
                object.addProperty("email",email);
                object.addProperty("feedback",feedback);
                Call<JsonObject> call = RetrofitInstance.getRetrofitInstance()
                        .create(MyApiEndpointInterface.class).sendEmail(object);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            Log.e("Response", AppUtils.getGsonInstance().toJson(response.body()));
                            viewBinding.edtMailId.setText("");
                            viewBinding.edtFeedback.setText("");
                            AppUtils.showCustomToast(FeedbackActivity.this,getString(R.string.msg_feedback_successfully));
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("Errorrr>>", t.toString());
                        viewBinding.edtMailId.setText("");
                        viewBinding.edtFeedback.setText("");
                        AppUtils.showCustomToast(FeedbackActivity.this,getString(R.string.msg_feedback_successfully));
                    }

                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppUtils.showCustomToast(FeedbackActivity.this,getString(R.string.check_internet));
        }

    }


}

package com.app.xinger.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.ExcludedContactDao;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.cluodStorage.CloudListener;
import com.app.xinger.ui.cluodStorage.CloudStorageActivity;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;
import com.app.xinger.ui.homescreen.HomeScreenActivity;
import com.app.xinger.ui.homescreen.listener.PermissionListener;
import com.app.xinger.util.Logger;
import com.aykuttasil.callrecord.receiver.CallRecordReceiver;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;
import static android.os.Environment.MEDIA_MOUNTED;

/**
 * Created by shubh on 21-Aug-17.
 */

public class AppUtils {

    protected final static String PrefName="CallRecorder";

    public final static String ENABLE_CALL_RECORDER="call recorder";
    public final static String ENABLE_OUTGOING_CALL="outgoing_Calls";
    public final static String ENABLE_INCOMING_CALLS="incoming_calls";
    public final static String SHOW_NOTIFICATION ="show_notification";
    public final static String RECORDED_INFO ="recorded_info";
    public final static String AUDIO_SOURCE="audio_source";
    public final static String AUDIO_SOURCE_NAME="audio_source_name";
    public final static String AUTO_SAVE_TO_CLOUD="auto_save_cloud";
    public final static String CALL_LIMIT="call_limit";

    public static Gson gson;
    private static Toast mToast=null;

    public static Gson getGsonInstance() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static String getFileName() {

        return "AUD_"+ System.currentTimeMillis();
    }

    /**
     * Set all boolean values in pref.
     * @param context
     * @param keyName
     * @param value
     */
    public static void setBooleanPrefValue(Context context,String keyName,boolean value) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        e.putBoolean(keyName, value);
        e.commit();
    }

    /**
     * get selected boolean pref with passing the name.
     * @param context
     * @param keyName
     */
    public static boolean getBooleanPrefValue(Context context,String keyName) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);
        if (keyName.equalsIgnoreCase(AppConstants.PREF_KEY_PRO_VERSION))
            return sp.getBoolean(keyName, false);
        return sp.getBoolean(keyName, true);
    }

    /**
     * Set all string values in pref.
     * @param context
     * @param keyName
     * @param value
     */
    public static void setStringPrefValue(Context context,String keyName,String value) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        e.putString(keyName, value);
        e.commit();
    }

    /**
     * get selected string pref with passing the name.
     * @param context
     * @param keyName
     */
    public static String getStringPrefValue(Context context,String keyName) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);
        if (keyName.equalsIgnoreCase(AppUtils.AUDIO_SOURCE_NAME))
            return sp.getString(keyName, "Mic");
        else
            return sp.getString(keyName, "");

    }


    /**
     * Set all integer values in pref.
     * @param context
     * @param keyName
     * @param value
     */
    public static void setIntegerPrefValue(Context context,String keyName,int value) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        e.putInt(keyName, value);
        e.commit();
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * get selected integer pref with passing the name.
     * @param context
     * @param keyName
     */
    public static int getIntegerPrefValue(Context context,String keyName) {
        SharedPreferences sp = context.getSharedPreferences(PrefName, MODE_PRIVATE);

        if (keyName.equalsIgnoreCase(AUDIO_SOURCE))
            return sp.getInt(keyName, MediaRecorder.AudioSource.MIC);
        else  if (keyName.equalsIgnoreCase(CALL_LIMIT))
            return sp.getInt(keyName, 100);
        else
        return sp.getInt(keyName, 0);
    }

    /**
     * Get date object in required format for provided time in millisecond.
     * @param milliSeconds
     * @param dateFormat
     * @return
     */
    public static String getDate(Long milliSeconds, String dateFormat) {
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

            // Create a calendar object that will convert the date and time value in milliseconds to date.

            formatter.setTimeZone(TimeZone.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            return formatter.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void ShowDeleteAlert(final Context context, final RecordedInfoModel record, final boolean isFinish) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog
                .setMessage(R.string.msg_delete_file)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        new File(record.getFilePath()).delete();
                        RecordedInfoDao.deleteSelectedRecord(record.get_id());
                        dialog.dismiss();
                        if (isFinish)
                            ((Activity)context).finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void   ShowDropBoxAlert(final Context context /*DbxAccountManager mDbxAcctMgr*/) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                         //api.getSession().unlink();
                        //OldDropboxClientFactory.setSDbxClient(null);
                        clearAccessToken(context);
                        ((CloudListener)context).onLogout();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    public static boolean hasToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(AppConstants.ACCOUNT_PREFS_NAME, MODE_PRIVATE);
        String accessToken = prefs.getString(AppConstants.ACCESS_TOKEN, null);
        return accessToken != null;
    }

    public static void clearAccessToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(AppConstants.ACCOUNT_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor e = prefs.edit();
        e.putString(AppConstants.ACCESS_TOKEN, null);
        e.commit();
    }



    public static void ShowExcludedDeleteAlert(final Context context, final String mobileNo) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog
                .setMessage(R.string.msg_excluded_number)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        ExcludedContactDao.deleteSelectedContact(mobileNo);
                        RecordedInfoDao.updateExcluded(mobileNo, false);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void ShowAlert(final Context context) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setMessage(R.string.msg_all_permission)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        ((PermissionListener)context).onPressOk();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void ShowDropBoxLoginAlert(final Context context) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setMessage(context.getString(R.string.msg_after_pro_version))
                .setPositiveButton(R.string.proceed, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent in = new Intent(context, CloudStorageActivity.class);
                        context.startActivity(in);
                        ((Activity)context).finish();
                    }
                })
                .setNegativeButton(R.string.do_it_latter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((Activity)context).finish();
                    }
                })
                .show();
    }

    public static void showExcludedAlertDialog(final Context context) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.dialog_title_excluded_no);
        dialog.setMessage("Please select options.")
                .setPositiveButton("Contacts", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent1 = new Intent(Intent.ACTION_PICK);
                        intent1.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                        ((Activity)context).startActivityForResult(intent1, 12);

                    }
                })
                .setNegativeButton("Non Contacts", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        showAddNumberAlert(context);
                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    public static void showAddNumberAlert(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        Rect displayRectangle = new Rect();
        Window window = ((Activity)context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.excluded_number_layout, null);
        layout.setMinimumWidth((int)(displayRectangle.width() * 0.8f));

        dialog.setContentView(layout);
        final EditText edt_number = (EditText)dialog.findViewById(R.id.edt_number);

        ((Button)dialog.findViewById(R.id.btn_add_number)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_number.getText().toString().trim().equalsIgnoreCase("")) {
                    AppUtils.showCustomToast(context,context.getResources().getString(R.string.toast_enter_number));
                }
                else if (!isNumeric(edt_number.getText().toString())){
                    AppUtils.showCustomToast(context,context.getResources().getString(R.string.toast_enter_valied_number));
                }
                else if (edt_number.getText().toString().length()<8){
                    AppUtils.showCustomToast(context,context.getResources().getString(R.string.toast_enter_valied_number));
                }
                else {
                    ExcludedContactModel model = new ExcludedContactModel();
                    model.setMobileNo(normalizeNumber(context,edt_number.getText().toString().trim()));
                    model.setContactName("");
                    ExcludedContactDao.insert(model);
                    RecordedInfoDao.updateExcluded(model.getMobileNo(),true);
                    dialog.dismiss();
                }
            }
        });

        ((Button)dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void ShowOkAlert(final Context context) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setMessage(R.string.msh_file_not_found)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        ((Activity)context).finish();
                    }
                })
                .show();
    }


    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /*Change app language*/
    public static void changeLanguage(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources()
                .updateConfiguration(
                        config,
                        context.getResources()
                                .getDisplayMetrics());
        Intent in = new Intent(context,HomeScreenActivity.class);
        context.startActivity(in);
        ((Activity)context).finish();
    }

    /**
     * Normalize a phone number by removing the characters other than digits. If
     * the given number has keypad letters, the letters will be converted to
     * digits first.
     *
     * @param phoneNumber the number to be normalized.
     * @return the normalized number.
     */
    public static String normalizeNumber(Context context,String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        int len = phoneNumber.length();
        for (int i = 0; i < len; i++) {
            char c = phoneNumber.charAt(i);
            // Character.digit() supports ASCII and Unicode digits (fullwidth, Arabic-Indic, etc.)
            int digit = Character.digit(c, 10);
            if (sb.length() == 0 && c == '0') {
                sb.append(getCountryDialCode(context));
            }  else if (sb.length() == 0 && c == '+') {
                sb.append(c);
            } else if (sb.length() == 0) {
                sb.append(getCountryDialCode(context)).append(digit);
            } else if (digit != -1 ) {
                sb.append(digit);
            }
         /*   else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                return normalizeNumber(PhoneNumberUtils.convertKeypadLettersToDigits(phoneNumber));
            }*/
        }
        return sb.toString();
    }


    public static void showCustomToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String getCountryDialCode(Context context){
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode=context.getResources().getStringArray(R.array.DialingCountryCode);
        for(int i=0; i<arrContryCode.length; i++){
            String[] arrDial = arrContryCode[i].split(",");
            if(arrDial[1].trim().equals(contryId.trim())){
                contryDialCode = arrDial[0];
                break;
            }
        }
        return "+"+contryDialCode;
    }


    public static int getContactIDFromNumber(String contactNumber,Context context) {
        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,contactNumber),new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while(contactLookupCursor.moveToNext()){
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }


    public static String getFileName(String path) {

        String[] str = path.split("/");
        return str[str.length-1];
    }


    /**
     * Static method to get an instance of material styled progress dialog
     *
     * @param mContext Context of the calling class
     * @return An instance of MaterialProgressDialog
     */
    public static MaterialProgressDialog getProgressDialogInstance(Context mContext) {
        MaterialProgressDialog mProgressDialog = new MaterialProgressDialog(mContext,
                mContext.getString(R.string.please_wait));
        mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mProgressDialog.setCancelable(false);
        return mProgressDialog;
    }

    public static boolean freeAvailableMemory() {
        StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        long   free   = (statFs.getAvailableBlocks() * statFs.getBlockSize());

        long Kb = 1  * 1024;
        long Mb = Kb * 1024;
        if (free>= Mb) {
            int space = ((int)(free/ Mb));
            if (space>40){
                return true;
            }
        }
        else {
            return false;
        }

        return false;
    }

    public static Uri createRecordFile(Context mContext,String suffix) throws IOException {
        File image = null;
        try {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String recordFileName = "AUD_" + timeStamp + "_";

            if (MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                File storageDir = new File(Environment.getExternalStorageDirectory(),
                        AppConstants.STORAGE_DIR);
                boolean parentCreationResult = storageDir.mkdirs();
                image = File.createTempFile(
                        recordFileName ,  /* prefix */
                        suffix,         /* suffix */
                        storageDir      /* directory */
                );

            } else {

                File storageDir = mContext.getFilesDir();
                image = File.createTempFile(
                        recordFileName ,  /* prefix */
                        suffix,         /* suffix */
                        storageDir      /* directory */
                );

            }

            // Save a file: path for use with ACTION_VIEW intents
            Log.d(CallRecordReceiver.class.getSimpleName(), "file:" + image.getAbsolutePath());
            return Uri.fromFile(image);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static boolean isShowFullAdd() {
        int randomNum = new Random().nextInt(100);
        if (randomNum % 3 == 0) {
            Logger.i("Advertisement", randomNum+" : Number True");
            return true;
        } else {
            Logger.i("Advertisement", randomNum+" : Number False");
            return false;
        }
    }

    public static String getAudioDuration(String filePath) {
        File yourFile= new File(filePath);

        if (yourFile.exists()) {
           try {
               MediaPlayer mp = new MediaPlayer();
               FileInputStream fs;
               FileDescriptor fd;
               fs = new FileInputStream(yourFile);
               fd = fs.getFD();
               mp.setDataSource(fd);
               mp.prepare();
               long dur =  mp.getDuration();;

               long aux = dur / 1000;
               int minute = (int) (aux / 60);
               int second = (int) (aux % 60);
               StringBuilder sb = new StringBuilder();

               if (minute>0) {
                   sb.append(minute>1? minute+" mins ": minute+" min ");
               }
               if (second>0){
                   sb.append(second > 1 ? second+" secs" : second+" sec");
               }
              /* String sDuration =
                       // Minutes
                       (minute < 10 ? "0"+minute+"m" : minute+"m")
                               + ":" +
                               // Seconds
                               (second < 10 ? "0"+second+"s" : second+"s");*/

               // close object
               mp.release();
               return sb.toString();
           }
           catch (Exception e) {
               e.printStackTrace();
           }
        }
        return "";
    }

    public static boolean IsFirstScreenofApp(Context context) {


        ActivityManager mgr = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskInfoList = mgr.getRunningTasks(10);

        if (taskInfoList.get(0).numActivities==1&&taskInfoList.get(0).topActivity.getClassName().equals(context.getClass().getName())) {
            return true;
        }
        else {
            return false;
        }
    }

}

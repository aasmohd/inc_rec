package com.app.xinger.ui.recordingscreen;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.xinger.R;
import com.app.xinger.model.BannerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ashik on 9/3/17.
 */

public class BannerAdapter extends PagerAdapter {

    Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<BannerModel> bannerList;

    public BannerAdapter(Context context, ArrayList<BannerModel> list) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannerList = list;
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.banner_item, container, false);
        ImageView img_banner  = (ImageView)itemView.findViewById(R.id.img_banner);
        Picasso.with(mContext)
                .load(bannerList.get(position).image)
                .into(img_banner);
        img_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (!bannerList.get(position).url.equalsIgnoreCase("")){
                   Intent i = new Intent(Intent.ACTION_VIEW);
                   i.setData(Uri.parse(bannerList.get(position).url));
                   mContext.startActivity(i);
               }
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}

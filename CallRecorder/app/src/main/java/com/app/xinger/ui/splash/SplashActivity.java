package com.app.xinger.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.xinger.R;
import com.app.xinger.apiInterface.MyApiEndpointInterface;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.service.RetrofitInstance;
import com.app.xinger.ui.homescreen.HomeScreenActivity;
import com.app.xinger.utils.AppUtils;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        GetBanners();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent in = new Intent(SplashActivity.this, HomeScreenActivity.class);
                startActivity(in);
                finish();
            }
        },1000);
    }

    /**
     * Get all banner events.
     */
    public void GetBanners() {

        if (AppUtils.isNetworkAvailable(SplashActivity.this)) {


            try {
                Call<JsonObject> call = RetrofitInstance.getRetrofitInstance()
                        .create(MyApiEndpointInterface.class).getBanners();

                call.enqueue(new Callback<JsonObject>() {

                    @Override

                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {

                            Log.e("Response", AppUtils.getGsonInstance().toJson(response.body()));
                            AppUtils.setStringPrefValue(SplashActivity.this,AppConstants.BANNER_RESPONSE ,response.body().toString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                        }

                    }
                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("Errorrr>>", t.toString());
                    }

                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }

    }
}

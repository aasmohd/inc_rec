package com.app.xinger;

import android.app.Application;

import com.app.xinger.db.XingerDBHelper;
import com.app.xinger.db.dao.ExcludedContactDao;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.google.android.gms.ads.MobileAds;


public class CallApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        XingerDBHelper.getdbInstance(getApplicationContext());
        RecordedInfoDao.init(getApplicationContext());
        ExcludedContactDao.init(getApplicationContext());
        MobileAds.initialize(getApplicationContext(), getString(R.string.banner_ad_unit_id));

        //CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("SFUIText-Light.ttf").setFontAttrId(R.attr.fontPath).build());
    }
}

package com.app.xinger.ui.homescreen.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ashish on 03/08/15.
 * This adapter is used in Offers fragment to show all the offers in list
 */
public class CallListViewCursorAdapter extends CursorAdapter {

    private final LayoutInflater layoutInflater;
    private Context mContext;
    ArrayList<Integer> multiselect_list = new ArrayList<>();
    public CallListViewCursorAdapter(Context mContext){
        super(mContext, null, true);
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view ;
        ViewHolder holder;
        holder = new ViewHolder();
        view = this.layoutInflater.inflate(R.layout.item_call_record, parent, false);

        holder.txt_name= (TextView)view.findViewById(R.id.txt_name);
        holder.txt_date= (TextView)view.findViewById(R.id.txt_date);
        holder.txt_time= (TextView)view.findViewById(R.id.txt_time);
        holder.img_user_pic= (ImageView)view.findViewById(R.id.img_user_pic);
        holder.img_favourite= (ImageView)view.findViewById(R.id.img_favourite);
        holder.img_sub_menu= (ImageView)view.findViewById(R.id.img_sub_menu);
        holder.img_checked= (ImageView)view.findViewById(R.id.img_checked);
        holder.rl_record= (RelativeLayout)view.findViewById(R.id.rl_record);
        holder.img_new= (ImageView)view.findViewById(R.id.img_new);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();
        final RecordedInfoModel record= DBUtil.getFromContentValue(cursor, RecordedInfoModel.class);
        if (cursor.getPosition()!=0) {
            final RecordedInfoModel prevrecord= DBUtil.getFromContentValue((Cursor)getItem(cursor.getPosition()-1), RecordedInfoModel.class);
            if (AppUtils.getDate(record.getTime(),
                    AppConstants.DATEFORMAT_dd_MMM_yyyy).equalsIgnoreCase(AppUtils.getDate(
                    prevrecord.getTime(),AppConstants.DATEFORMAT_dd_MMM_yyyy))) {
                holder.txt_date.setVisibility(View.GONE);
            }
            else {
                holder.txt_date.setVisibility(View.VISIBLE);
            }
        }
        else {
            holder.txt_date.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(record.getContactName())) {
            holder.txt_name.setText(record.getMobileNo().trim());
        }
        else {
            holder.txt_name.setText(record.getContactName().trim());
        }
        if (!record.getProfilePic().equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(record.getProfilePic())
                    .into(holder.img_user_pic);
        }
        else {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            if (!TextUtils.isEmpty(record.getContactName())) {
                holder.img_user_pic.setImageDrawable(TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)/* size in px */
                        .toUpperCase()
                        .endConfig()
                        .buildRound(record.getContactName().toUpperCase().charAt(0)+"",generator.getColor(record.getMobileNo().trim())));
            }
            else{
                holder.img_user_pic.setImageDrawable(TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)/* size in px */
                        .toUpperCase()
                        .endConfig()
                        .buildRound("N",generator.getColor(record.getMobileNo().trim())));
            }

        }
        Log.e("IsInComing>>",record.getIsInComing()+"");
        if (record.getIsInComing()==1){
            holder.txt_name.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.incall,0);
        }
        else {
            holder.txt_name.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.outcall,0);
        }

        if (record.getIsFavourite().equalsIgnoreCase("1")) {
            holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starfilled));
        }
        else {
            holder.img_favourite.setImageDrawable(context.getResources().getDrawable(R.drawable.starstroke));
        }
        holder.img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (record.getIsFavourite().equalsIgnoreCase("1")) {
                            record.setIsFavourite("0");
                            holder.img_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.starstroke));
                        }
                        else {
                            record.setIsFavourite("1");
                            holder.img_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.starfilled));
                        }
                        RecordedInfoDao.updateFavourite(record);
                    }
                },1);
            }
        });
        holder.txt_time.setText(AppUtils.getDate(record.getTime(),
                AppConstants.DATEFORMAT_HH_mm_a)+" | "+AppUtils.getAudioDuration(record.getFilePath()));
        holder.txt_date.setText(AppUtils.getDate(record.getTime(),
                AppConstants.DATEFORMAT_dd_MMM_yyyy));

        holder.img_sub_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyPopUpMenu().ShowPopUp(mContext,record,holder.img_sub_menu);
            }
        });
       /* holder.rl_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(mContext, RecordingActivity.class);
                in.putExtra(AppConstants.RECORD_DATA,AppUtils.getGsonInstance().toJson(record));
                mContext.startActivity(in);
            }
        });*/

       if (isSelected(record.get_id())){
           holder.rl_record.setBackgroundColor(context.getResources().getColor(R.color.gray_selector));
           holder.img_user_pic.setVisibility(View.GONE);
           holder.img_checked.setVisibility(View.VISIBLE);
           holder.img_favourite.setClickable(false);
           holder.img_sub_menu.setClickable(false);
       }
       else {
           holder.rl_record.setBackgroundColor(context.getResources().getColor(R.color.white));
           holder.img_user_pic.setVisibility(View.VISIBLE);
           holder.img_checked.setVisibility(View.GONE);
           holder.img_favourite.setClickable(true);
           holder.img_sub_menu.setClickable(true);
       }

       if (record.getIsNew().equalsIgnoreCase("1")) {
           holder.img_new.setVisibility(View.VISIBLE);
       }
       else {
           holder.img_new.setVisibility(View.GONE);
       }
    }

    private class ViewHolder{
        TextView txt_name,txt_time,txt_date;
        ImageView img_user_pic,img_favourite,img_sub_menu,img_checked,img_new;
        RelativeLayout rl_record;
    }


    public ArrayList<Integer> getMultiselect_list() {
        return multiselect_list;
    }

    public void removeAllSelection() {
        multiselect_list.clear();
        notifyDataSetChanged();
    }


    public boolean isSelected(int recordId) {

        for (int i = 0; i <multiselect_list.size() ; i++) {

            if (recordId==multiselect_list.get(i)){
                return true;
            }
        }
        return false;
    }



}

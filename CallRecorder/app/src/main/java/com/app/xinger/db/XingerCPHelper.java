package com.app.xinger.db;

import android.net.Uri;

import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;

public class XingerCPHelper {


    public static class Uris {

        public final static Uri URI_APP_TABLE = Uri.withAppendedPath(XingerContentProviderData.CONTENT_URI,RecordedInfoModel.class.getSimpleName());
        public final static Uri URI_EXCLUDED_CONTACT= Uri.withAppendedPath(XingerContentProviderData.CONTENT_URI,ExcludedContactModel.class.getSimpleName());
       }

}

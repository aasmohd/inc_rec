package com.app.xinger.cloud;

/**
 * Created by shubh on 28-Nov-17.
 */

import android.content.SharedPreferences;

import com.app.xinger.constants.AppConstants;
import com.app.xinger.ui.BaseActivity;
import com.dropbox.core.android.Auth;


/**
 * Base class for Activities that require auth tokens
 * Will redirect to auth flow if needed
 */
public abstract class DropboxActivity extends BaseActivity {

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(AppConstants.ACCOUNT_PREFS_NAME, MODE_PRIVATE);
        String accessToken = prefs.getString(AppConstants.ACCESS_TOKEN , null);
        if (accessToken == null) {
            accessToken = Auth.getOAuth2Token();
            if (accessToken != null) {
                prefs.edit().putString(AppConstants.ACCESS_TOKEN, accessToken).apply();
                initAndLoadData(accessToken);
            }
        } else {
            initAndLoadData(accessToken);
        }

        String uid = Auth.getUid();
        String storedUid = prefs.getString("user-id", null);
        if (uid != null && !uid.equals(storedUid)) {
            prefs.edit().putString("user-id", uid).apply();
        }
    }

    private void initAndLoadData(String accessToken) {
        DropboxClientFactory.init(accessToken);
    }

    protected abstract void loadData();



}
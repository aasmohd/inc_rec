package com.app.xinger.utils;

import android.content.Context;

import com.aykuttasil.callrecord.CallRecord;

/**
 * Created by shubh on 29-Aug-17.
 */

public class CallRecorderInstance {

    private  CallRecord callRecord;
    private static CallRecorderInstance instance;
    private int recordId;

    public CallRecorderInstance(Context context) {

    }

    public static CallRecorderInstance getCallRecordInstance(Context context) {
        if (instance == null) {
            instance =new CallRecorderInstance(context);
        }
        return instance;
    }

    public CallRecord getCallRecord() {
        return callRecord;
    }

    public void setCallRecord(CallRecord callRecord) {
        if(this.callRecord == null)
            this.callRecord = callRecord;
    }

    public void destroy() {
        this.callRecord = null;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}

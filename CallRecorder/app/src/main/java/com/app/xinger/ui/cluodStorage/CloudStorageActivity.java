package com.app.xinger.ui.cluodStorage;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import com.app.xinger.R;
import com.app.xinger.cloud.DropboxActivity;
import com.app.xinger.cloud.DropboxClientFactory;
import com.app.xinger.cloud.UploadFileTask;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.ActivityCloudStorageBinding;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.ui.homescreen.listener.HomePresenter;
import com.app.xinger.utils.AppUtils;
import com.dropbox.core.android.Auth;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

;

public class CloudStorageActivity extends DropboxActivity implements CloudListener{

    ActivityCloudStorageBinding viewBinding;
   // private DbxAccountManager mDbxAcctMgr;
    //DropboxAPI<AndroidAuthSession> mApi;
    private static final int REQUEST_LINK_TO_DBX = 1010;
    private static final String ACCOUNT_PREFS_NAME = "call_recorder_prefs";
    private static final String ACCESS_KEY_NAME = "ACCESS_KEY";
    private static final String ACCESS_SECRET_NAME = "ACCESS_SECRET";

    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_cloud_storage);
       // mApi = new DropboxAPI<AndroidAuthSession>(session);
       // mDbxAcctMgr = DbxAccountManager.getInstance(this.getApplicationContext(), appKey, appSecret);
        viewBinding.switchAutoSave.setChecked(AppUtils.getBooleanPrefValue(CloudStorageActivity.this,AppUtils.AUTO_SAVE_TO_CLOUD));
        if(AppUtils.hasToken(CloudStorageActivity.this)) {
            viewBinding.switchAutoSave.setEnabled(true);
            viewBinding.switchAutoSave.setAlpha(1);
            viewBinding.txtLogin.setText(R.string.logout);
            viewBinding.txtLoginDesc.setText(R.string.logout_to_dropbox);
        }
        else {
            viewBinding.switchAutoSave.setEnabled(false);
            viewBinding.txtLogin.setText(getString(R.string.login));
            viewBinding.switchAutoSave.setAlpha(0.5f);
            viewBinding.txtLoginDesc.setText(getString(R.string.login_to_dropbox));
        }

        viewBinding.rlLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.hasToken(CloudStorageActivity.this)) {
                    AppUtils.ShowDropBoxAlert(CloudStorageActivity.this);
                }
                else {
                   /* mDbxAcctMgr.startLink((Activity)CloudStorageActivity.this, REQUEST_LINK_TO_DBX);*/
                    Auth.startOAuth2Authentication(CloudStorageActivity.this,AppConstants.APP_KEY);
                }

            }
        });

        viewBinding.toolbar.setTitleTextColor(0xFFFFFFFF);
        viewBinding.toolbar.setTitle(getString(R.string.cloud_account));
        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        viewBinding.rlSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (AppUtils.hasToken(CloudStorageActivity.this)) {
                   //"Already Sync";
                   Cursor cursor = RecordedInfoDao.getAllRecordFile(0);
                   if (cursor .getCount()>0){
                       showProgressDialog(true);
                       new UploadFileTask(CloudStorageActivity.this, DropboxClientFactory.getClient(),true);
                   }
                   else
                       AppUtils.showCustomToast(CloudStorageActivity.this,getString(R.string.already_sync));
               }
               else {
                   AppUtils.showCustomToast(CloudStorageActivity.this,getString(R.string.toast_sync_recording));
               }
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        viewBinding.adView.loadAd(adRequest);

        if (AppUtils.isShowFullAdd()) {
            mInterstitialAd = new InterstitialAd(CloudStorageActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }

        viewBinding.switchAutoSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppUtils.setBooleanPrefValue(CloudStorageActivity.this,AppUtils.AUTO_SAVE_TO_CLOUD,
                        viewBinding.switchAutoSave.isChecked());
                viewBinding.switchAutoSave.setChecked(AppUtils.getBooleanPrefValue(CloudStorageActivity.this,
                        AppUtils.AUTO_SAVE_TO_CLOUD));
            }
        });
        viewBinding.switchAutoSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppUtils.setBooleanPrefValue(CloudStorageActivity.this,AppUtils.AUTO_SAVE_TO_CLOUD,
                        viewBinding.switchAutoSave.isChecked());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppUtils.hasToken(CloudStorageActivity.this)) {
            viewBinding.switchAutoSave.setEnabled(true);
            viewBinding.switchAutoSave.setAlpha(1);
            viewBinding.txtLogin.setText(R.string.logout);
            viewBinding.txtLoginDesc.setText(R.string.logout_to_dropbox);
        }
        else {
            viewBinding.switchAutoSave.setEnabled(false);
            viewBinding.txtLogin.setText(getString(R.string.login));
            viewBinding.switchAutoSave.setAlpha(0.5f);
            viewBinding.txtLoginDesc.setText(getString(R.string.login_to_dropbox));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if (requestCode == REQUEST_LINK_TO_DBX) {
            if (resultCode == Activity.RESULT_OK) {
                HomePresenter.ShowSyncAlert(CloudStorageActivity.this);
                viewBinding.switchAutoSave.setEnabled(true);
                viewBinding.switchAutoSave.setAlpha(1);
                viewBinding.txtLogin.setText(R.string.logout);
                viewBinding.txtLoginDesc.setText(R.string.logout_to_dropbox);
            } else {
                AppUtils.showCustomToast(CloudStorageActivity.this,"Link to Dropbox failed or was cancelled.");
            }
       /* } else {
            super.onActivityResult(requestCode, resultCode, data);
        }*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void hideProgress() {
        showProgressDialog(false);
    }

    @Override
    public void uploadFileOnServer() {
        viewBinding.rlSync.performClick();
    }

    @Override
    public void onLogout() {
        viewBinding.switchAutoSave.setEnabled(false);
        viewBinding.switchAutoSave.setAlpha(0.5f);
        viewBinding.txtLogin.setText(R.string.login);
        viewBinding.txtLoginDesc.setText(R.string.login_to_dropbox);
    }

    @Override
    protected void loadData() {

    }
}

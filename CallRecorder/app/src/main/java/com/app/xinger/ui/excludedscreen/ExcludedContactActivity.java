package com.app.xinger.ui.excludedscreen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.app.xinger.R;
import com.app.xinger.databinding.ActivityExcludedContactBinding;
import com.app.xinger.db.dao.ExcludedContactDao;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.utils.AppUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class ExcludedContactActivity extends BaseActivity  implements LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener{

    ActivityExcludedContactBinding viewBinding;
    ExcludedContactCursorAdapter adapter;
    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_excluded_contact);

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(ExcludedContactActivity.this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        viewBinding.recyclerView.setHasFixedSize(true);
        viewBinding.recyclerView.setLayoutManager(mLinearLayoutManager);
        adapter = new ExcludedContactCursorAdapter(ExcludedContactActivity.this);
        viewBinding.recyclerView.setAdapter(adapter);

        getSupportLoaderManager().initLoader(0, null, this);

        viewBinding.imgAdd.setOnClickListener(this);
        viewBinding.imgBack.setOnClickListener(this);
        AdRequest adRequest = new AdRequest.Builder().build();
        viewBinding.adView.loadAd(adRequest);

        if (AppUtils.isShowFullAdd()) {
            mInterstitialAd = new InterstitialAd(ExcludedContactActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return ExcludedContactDao.getCursorLoader();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            adapter.swapCursor(data);
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back :
                finish();
                break;

            case R.id.img_add :
                AppUtils.showExcludedAlertDialog(ExcludedContactActivity.this);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK&& requestCode==12) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
            Cursor cursor = getContentResolver()
                    .query(contactUri, projection, null, null, null);
            cursor.moveToFirst();

            if (!ExcludedContactDao.isPhoneExist(AppUtils.normalizeNumber(ExcludedContactActivity.this,cursor.getString(cursor.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.NUMBER))))) {

                ExcludedContactModel model = new ExcludedContactModel();
                model.setMobileNo(AppUtils.normalizeNumber(ExcludedContactActivity.this,
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))));
                model.setContactName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                ExcludedContactDao.insert(model);
                RecordedInfoDao.updateExcluded(AppUtils.normalizeNumber(ExcludedContactActivity.this,
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))),true);
            }
            else {
                AppUtils.showCustomToast(ExcludedContactActivity.this,getString(R.string.toast_already_added));
            }
            /*Update exclude number in record table*/

            // TODO Fetch other Contact details as you want to use

        }
    }


}

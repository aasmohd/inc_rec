package com.app.xinger.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.app.xinger.R;
import com.app.xinger.databinding.ActivityDisclaimerBinding;

public class DisclaimerActivity extends BaseActivity {

    ActivityDisclaimerBinding viewBinding;

    String str = "<div class=\"xinger-text\">\n" +
            "  <div class=\"container\">\n" +
            "      <p style=\"font-size:15px;\"><span style=\"display:block;font-size:30px;\">Xinger App</span> is only meant to be used for personal use and requires the consent of both the persons involved. Recording calls is prohibited in certain countries, and any offense is liable to local jurisdiction.</p>\n" +
            "   <p style=\"font-size:15px;\"><span style=\"display:block;font-size:30px;\">Appstudz</span>doesn&rsquo;t promote any unlawful acts, and takes no responsibility in any situation wherein there is unlawful use of the app. </p>\n" +
            "  </div>\n" +
            "</div>";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding =(ActivityDisclaimerBinding)DataBindingUtil.setContentView(this, R.layout.activity_disclaimer);
        viewBinding.toolbar.setTitle("Disclaimer");
        viewBinding.toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        viewBinding.webview.clearFormData();
        viewBinding.webview.clearHistory();
        viewBinding.webview.clearCache(true);
        viewBinding.webview.loadData(str, "text/html", "UTF-8");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

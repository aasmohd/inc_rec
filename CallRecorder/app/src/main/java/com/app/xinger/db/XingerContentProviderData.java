package com.app.xinger.db;

import android.net.Uri;

import com.app.xinger.constants.AppConstants;


public class XingerContentProviderData {

    public static final String AUTHORITY = AppConstants.CONTENT_PROVIDER_AUTHORITY_NAME();

    // The URI scheme used for content URIs
    public static final String SCHEME = "content";

    /**
     * The DataProvider content URI
     */
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    public static final int ID_APP_TABLE = 1;
    public static final int ID_EXCLUDED_TABLE= 2;
}

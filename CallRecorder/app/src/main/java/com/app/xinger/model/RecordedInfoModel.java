package com.app.xinger.model;

import com.app.xinger.dbhelper.DBClass;

import java.io.Serializable;

/**
 * Created by shubh on 24-Aug-17.
 */

public class RecordedInfoModel implements DBClass, Serializable {

    public Integer _id;
    public String filePath,
            duration,
            isFavourite,
            mobileNo,
            contactName,
            profilePic;
    public Long time;
    public Integer isInComing;
    public Integer isCallEnd;
    public Boolean isExcluded;
    public Integer contactId;
    public Integer isOncloud;
    public String isNew;


    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getIsCallEnd() {
        return isCallEnd;
    }

    public void setIsCallEnd(Integer isCallEnd) {
        this.isCallEnd = isCallEnd;
    }

    public RecordedInfoModel() {
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getDuration() {
        if (duration == null)
            duration = "0";
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getContactName() {
        if (contactName == null)
            contactName = "";
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getProfilePic() {
        if (profilePic == null)
            profilePic = "";
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public int getIsInComing() {
        return isInComing;
    }

    public void setIsInComing(Integer isInComing) {
        this.isInComing = isInComing;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public Boolean getIsExcluded() {
        return isExcluded;
    }

    public void setIsExcluded(Boolean excluded) {
        isExcluded = excluded;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public Integer getIsOncloud() {
        return isOncloud;
    }

    public void setIsOncloud(Integer isOncloud) {
        this.isOncloud = isOncloud;
    }
}

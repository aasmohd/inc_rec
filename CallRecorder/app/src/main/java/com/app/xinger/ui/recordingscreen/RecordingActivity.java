package com.app.xinger.ui.recordingscreen;

import android.app.NotificationManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.app.xinger.R;
import com.app.xinger.audioplayer.MyAudioPlayer;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.databinding.ActivityRecordingBinding;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.BannerResponse;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.BaseActivity;
import com.app.xinger.ui.homescreen.HomeScreenActivity;
import com.app.xinger.utils.AppUtils;
import com.google.android.gms.ads.AdRequest;

import java.io.File;

public class RecordingActivity extends BaseActivity {
    ActivityRecordingBinding viewBinding;
    RecordedInfoModel record;
    int seekBackwardTime=2000;
    int seekForwardTime=2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this,R.layout.activity_recording);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(AppConstants.NOTIFICATION_ID);
        try {
             record = AppUtils.getGsonInstance().fromJson(getIntent().getStringExtra(AppConstants.RECORD_DATA),RecordedInfoModel.class);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        viewBinding.toolbar.setTitle(R.string.call_player);
        if (new File(record.getFilePath()).exists()){
            PlayRecordFile(Uri.parse(record.getFilePath()));
        }
        else {
            AppUtils.ShowOkAlert(RecordingActivity.this);
        }

        if (record.getContactName().equalsIgnoreCase("")) {
            viewBinding.txtContactName.setVisibility(View.GONE);
        }
        else {
            viewBinding.txtContactName.setText(record.getContactName());
            viewBinding.txtContactName.setVisibility(View.VISIBLE);
        }
        viewBinding.txtContactNumber.setText(record.getMobileNo());
        viewBinding.txtSize.setText(getFolderSize(new File(record.getFilePath())));
        viewBinding.txtDateTime.setText(AppUtils.getDate(record.getTime(),AppConstants.DATEFORMAT_DD_MMM_YYYY_HH_MM_A));

        setSupportActionBar(viewBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        AdRequest adRequest = new AdRequest.Builder().build();
        viewBinding.adView.loadAd(adRequest);
        BannerResponse response = AppUtils.getGsonInstance().fromJson(AppUtils.getStringPrefValue(RecordingActivity.this,AppConstants.BANNER_RESPONSE),BannerResponse.class);

        BannerAdapter adapter = new BannerAdapter(RecordingActivity.this,response.result.banners);
        viewBinding.viewPager.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recording_menu, menu);

        if (record.getIsFavourite().equalsIgnoreCase("1")) {
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.fill_star));
        }
        else {
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.star));
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_favourite) {
           new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   if (record.getIsFavourite().equalsIgnoreCase("1")) {
                       record.setIsFavourite("0");
                       item.setIcon(getResources().getDrawable(R.drawable.star));
                   }
                   else {
                       record.setIsFavourite("1");
                       item.setIcon(getResources().getDrawable(R.drawable.fill_star));
                   }
                   RecordedInfoDao.updateFavourite(record);
               }
           },1);
            return true;
        }
        else  if (id == R.id.action_share) {
            Uri uri = FileProvider.getUriForFile(this, AppConstants.FILEPROVIDER, new File(record.getFilePath()));
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("audio/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, getString(R.string.share_intent_msg)));
            return true;
        }
        else  if (id == R.id.action_delete) {
            AppUtils.ShowDeleteAlert(RecordingActivity.this,record,true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyAudioPlayer.getInstance().release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyAudioPlayer.getInstance().pause();
    }

    public static String getFolderSize(File f) {
        long size = 0;
        size=f.length();
        String value=null;
        long Filesize=size/1024;//call function and convert bytes into Kb
        if(Filesize>=1024)
            value=Filesize/1024+" Mb";
        else
            value=Filesize+" Kb";

        return value;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (AppUtils.IsFirstScreenofApp(RecordingActivity.this))
            startActivity(new Intent(RecordingActivity.this, HomeScreenActivity.class));

        finish();
    }


    public void PlayRecordFile(Uri uri){
        MyAudioPlayer.getInstance()
                .init(RecordingActivity.this, uri)
                .setPlayView(viewBinding.myplayer.btnPlay)
                .setPauseView(viewBinding.myplayer.btnPause)
                .setSeekBar(viewBinding.myplayer.seekBar)
                .setRuntimeView(viewBinding.myplayer.txtCurrentDuration)
                .setTotalTimeView(viewBinding.myplayer.txtTotalDuration);
        MyAudioPlayer.getInstance().play();

        MyAudioPlayer.getInstance().addOnCompletionListener( new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // do you stuff
                viewBinding.myplayer.btnPause.setVisibility(View.GONE);
                viewBinding.myplayer.btnPlay.setVisibility(View.VISIBLE);
                viewBinding.myplayer.btnPrev.setClickable(false);
                viewBinding.myplayer.btnNext.setClickable(false);

            }
        });



        MyAudioPlayer.getInstance().addOnPlayClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Lights-Camera-Action. Lets dance.
                viewBinding.myplayer.btnPrev.setClickable(true);
                viewBinding.myplayer.btnNext.setClickable(true);
                viewBinding.myplayer.btnPause.setVisibility(View.VISIBLE);
                viewBinding.myplayer.btnPlay.setVisibility(View.GONE);
            }
        });

        MyAudioPlayer.getInstance().addOnPauseClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Your on audio pause stuff.
                viewBinding.myplayer.btnPause.setVisibility(View.GONE);
                viewBinding.myplayer.btnPlay.setVisibility(View.VISIBLE);
            }
        });

        viewBinding.myplayer.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPosition = MyAudioPlayer.getInstance().mMediaPlayer.getCurrentPosition();
                // check if seekForward time is lesser than song duration

                if (currentPosition== MyAudioPlayer.getInstance().mMediaPlayer.getDuration())
                    return;

                if (currentPosition + seekForwardTime <=  MyAudioPlayer.getInstance().mMediaPlayer.getDuration()) {
                    // forward song
                    //  mp.seekTo(currentPosition + seekForwardTime);
                    viewBinding.myplayer.seekBar.setProgress(currentPosition+seekForwardTime);
                    MyAudioPlayer.getInstance().mMediaPlayer.seekTo(currentPosition+seekForwardTime);
                } else {
                    // forward to end position
                    viewBinding.myplayer.seekBar.setProgress(MyAudioPlayer.getInstance().mMediaPlayer.getDuration());
                    MyAudioPlayer.getInstance().mMediaPlayer.seekTo(MyAudioPlayer.getInstance().mMediaPlayer.getDuration());
                    MyAudioPlayer.getInstance().play();
                }
            }
        });


        viewBinding.myplayer.btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int currentPosition = MyAudioPlayer.getInstance().mMediaPlayer.getCurrentPosition();
                // check if seekForward time is lesser than song duration

                if (currentPosition - seekBackwardTime >= 0) {
                    MyAudioPlayer.getInstance().mMediaPlayer.seekTo(currentPosition-seekBackwardTime);
                    viewBinding.myplayer.seekBar.setProgress(currentPosition-seekBackwardTime);
                } else {
                    // forward to end position
                    viewBinding.myplayer.seekBar.setProgress(0);
                    MyAudioPlayer.getInstance().mMediaPlayer.seekTo(0);
                }
            }
        });

    }


}

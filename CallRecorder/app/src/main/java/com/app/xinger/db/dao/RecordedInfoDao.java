package com.app.xinger.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.xinger.db.XingerCPHelper;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;

import java.util.List;


public class  RecordedInfoDao {

	private static Context mContext;

	public static void init(Context context) {
		mContext = context;
	}

	public static int insert(RecordedInfoModel brand) {
		ContentValues values;
		values = DBUtil.getContentValues(brand);
		Uri uri = mContext.getContentResolver().insert(XingerCPHelper.Uris.URI_APP_TABLE, values);
		int localIndexId = 0;
		if (uri != null) {
			localIndexId = Integer.parseInt(uri.getLastPathSegment());
		}
		return localIndexId;
	}

	public static void update(RecordedInfoModel brand) {
		ContentValues values = new ContentValues();
		values.put("_id", brand.get_id());
		values.put("isCallEnd", brand.getIsCallEnd());
		//values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_APP_TABLE, values, null, null);
	}

	public static void updateFavourite(RecordedInfoModel brand) {
		ContentValues values = new ContentValues();
		values.put("_id", brand.get_id());
		values.put("isFavourite", brand.getIsFavourite());
		//values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_APP_TABLE, values, null, null);
	}

	public static void updateExcluded(String phoneNum,  boolean isExcluded) {
		ContentValues values = new ContentValues();
	//	values.put("mobileNo", phoneNum);
		values.put("isExcluded", isExcluded);
		//values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_APP_TABLE, values,  "mobileNo ='"+phoneNum +"'", null);
	}

	public static void updateFileStatus(String filePath,  Integer isOncloud,String _id) {
		ContentValues values = new ContentValues();
		values.put("filePath", filePath);
		values.put("isOncloud", isOncloud);
		//values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_APP_TABLE, values,  "_id ='"+_id +"'", null);
	}

    public static Loader<Cursor> getCursorLoader(String searchText) {
		//searchText = searchText.replaceAll("'","\'");
		if (searchText.equalsIgnoreCase("")) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1", null,"time DESC");
		}
		else if (AppUtils.isNumeric(searchText)){
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and mobileNo LIKE ? ", new String[]{"%"+searchText+"%"},"time DESC");
		}
		else {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and contactName LIKE ?",  new String[]{"%"+searchText+"%"},"time DESC");
		}

	}

	public static Loader<Cursor> getFilteredCursorLoader(int isIncoming,String searchText) {
		if (searchText.equalsIgnoreCase("")) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isInComing="+isIncoming, null,"time DESC");
		}
		else if (AppUtils.isNumeric(searchText)) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isInComing="+isIncoming+" and mobileNo  LIKE ?",
					new String[]{"%"+searchText+"%"},"time DESC");
		}
		else {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isInComing="+isIncoming+" and contactName  LIKE ?",
					new String[]{"%"+searchText+"%"},"time DESC");
		}

	}

	public static Loader<Cursor> getFavouriteLoader(String searchText) {
		searchText = searchText.replaceAll("'","\'");
		if (searchText.equalsIgnoreCase("")) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isFavourite=\'1\'", null,"time DESC");
		}
		else if (AppUtils.isNumeric(searchText)) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isFavourite=\'1\' and mobileNo  LIKE ?",
					new String[]{"%"+searchText+"%"},"time DESC");
		}
		else {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_APP_TABLE, null,"isCallEnd= 1 and isFavourite=\'1\'  and contactName  LIKE ?",
					new String[]{"%"+searchText+"%"},"time DESC");
		}

	}

	public static void delete() {
        mContext.getContentResolver().delete(XingerCPHelper.Uris.URI_APP_TABLE, null, null);
    }

	public static Cursor getAllRecordFile(int isOnCloud) {
		return mContext.getContentResolver().query(XingerCPHelper.Uris.URI_APP_TABLE, null,"isOncloud="+isOnCloud,null, "time DESC");
	}

	public static Cursor checkRecordCount() {
		return mContext.getContentResolver().query(XingerCPHelper.Uris.URI_APP_TABLE, null,null,null, null);
	}

	public static Cursor getSelectedRecord(int id) {
		return mContext.getContentResolver().query(XingerCPHelper.Uris.URI_APP_TABLE, null,"_id="+id,null, null);
	}

	public static void deleteSelectedRecord(int recordId) {
		mContext.getContentResolver().delete(XingerCPHelper.Uris.URI_APP_TABLE, "_id="+recordId, null);
	}

	public static void deleteLeastRecord() {
		Cursor cursor =mContext.getContentResolver().query(XingerCPHelper.Uris.URI_APP_TABLE,
				new String[] {"MIN(time) AS min_time"}, null, null, null);
		cursor.moveToFirst();
		mContext.getContentResolver().delete(XingerCPHelper.Uris.URI_APP_TABLE,"time="+cursor.getLong(0), null);
	}

	public static void bulkInsert(List<RecordedInfoDao> appModelList) {
		ContentValues [] contentValues = DBUtil.getContentValuesList(appModelList);
		mContext.getContentResolver().bulkInsert(XingerCPHelper.Uris.URI_APP_TABLE, contentValues);
	}

	public static void updateAllRecord() {
		ContentValues values = new ContentValues();
		values.put("isNew", "0");
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_APP_TABLE,values,"", null);
	}
}
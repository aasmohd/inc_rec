package com.app.xinger.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.app.xinger.db.XingerCPHelper;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;

import java.util.List;


public class ExcludedContactDao {

	private static Context mContext;

	public static void init(Context context) {
		mContext = context;
	}

	public static int insert(ExcludedContactModel brand) {
		int localIndexId = 0;
		if(!isPhoneExist(brand.getMobileNo())){
			ContentValues values;
			values = DBUtil.getContentValues(brand);
			Uri uri = mContext.getContentResolver().insert(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, values);
			if (uri != null) {
				localIndexId = Integer.parseInt(uri.getLastPathSegment());
			}
		}
		return localIndexId;
	}

	public static boolean isPhoneExist(String phoneNum) {

		Cursor cursor = mContext.getContentResolver().query(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null, "mobileNo='"+phoneNum+"'", null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return true;
		}
		return false;
	}

	public static void update(ExcludedContactModel brand) {
		ContentValues values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, values, null, null);
	}

	public static void updateFavourite(ExcludedContactModel brand) {
		ContentValues values = new ContentValues();
		//values.put("_id", brand.get_id());
		//values.put("isFavourite", brand.getIsFavourite());
		//values = DBUtil.getContentValues(brand);
		mContext.getContentResolver().update(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, values, null, null);
	}

    public static Loader<Cursor> getCursorLoader() {
		return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,null, null,null);
	}

	public static Loader<Cursor> getFilteredCursorLoader(int isIncoming,String searchText) {

		if (searchText.equalsIgnoreCase("")) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isInComing="+isIncoming, null,null);
		}
		else if (AppUtils.isNumeric(searchText)) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isInComing="+isIncoming+" and mobileNo  LIKE '%"+searchText+"%'", null,null);
		}
		else {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isInComing="+isIncoming+" and contactName  LIKE '%"+searchText+"%'", null,null);
		}

	}

	public static Loader<Cursor> getFavouriteLoader(String searchText) {
		if (searchText.equalsIgnoreCase("")) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isFavourite=\'1\'", null,null);
		}
		else if (AppUtils.isNumeric(searchText)) {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isFavourite=\'1\' and mobileNo  LIKE '%"+searchText+"%'", null,null);
		}
		else {
			return new CursorLoader(mContext, XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null,"isCallEnd= 1 and isFavourite=\'1\'  and contactName  LIKE '%"+searchText+"%'", null,null);
		}

	}

	public static void delete() {
        mContext.getContentResolver().delete(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, null, null);
    }

	public static void deleteSelectedContact(String mobileNo) {
		mContext.getContentResolver().delete(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, "mobileNo='"+mobileNo+"'", null);
	}




	public static void bulkInsert(List<ExcludedContactDao> appModelList) {
		ContentValues [] contentValues = DBUtil.getContentValuesList(appModelList);
		mContext.getContentResolver().bulkInsert(XingerCPHelper.Uris.URI_EXCLUDED_CONTACT, contentValues);
	}
}
package com.app.xinger.ui.homescreen.listener;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.app.xinger.ui.cluodStorage.CloudListener;

/**
 * Created by shubh on 05-Sep-17.
 */

public class HomePresenter {


    public static void ShowSyncAlert(final Context context) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setMessage("Sync all recording now?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ((CloudListener)context).uploadFileOnServer();
                        dialog.dismiss();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }




}

package com.app.xinger.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.os.Environment;
import android.provider.ContactsContract;

import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.utils.AppUtils;
import com.app.xinger.utils.CallRecorderInstance;
import com.aykuttasil.callrecord.CallRecord;
import com.aykuttasil.callrecord.receiver.CallRecordReceiver;

/**
 * Created by shubh on 17-Aug-17.
 */

public class MyCallingreceiver extends BroadcastReceiver {

    public static CallRecord callRecord;

    @Override
    public void onReceive(Context context, Intent intent)  {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CallRecordReceiver.ACTION_IN);
            intentFilter.addAction(CallRecordReceiver.ACTION_OUT);

            MyPhoneReceiver mCallRecordReceiver = new MyPhoneReceiver();
            context.getApplicationContext().registerReceiver(mCallRecordReceiver, intentFilter);

    }

    public RecordedInfoModel getContactName(final String phoneNumber, Context context) {
        //Uri uri= Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
                ContactsContract.CommonDataKinds.Phone._ID};
        RecordedInfoModel record = new RecordedInfoModel();
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, ContactsContract.CommonDataKinds.Phone.NUMBER + "=?",
                new String[]{phoneNumber}, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                record.setContactName(cursor.getString(0));
                record.setProfilePic(cursor.getString(1));
                record.setContactId(cursor.getInt(2));
            } else {
                record.setContactName("");
                record.setProfilePic("");
                record.setContactId(0);
            }
            record.setMobileNo(phoneNumber);
            cursor.close();
        }
        return record;
    }

    private void startRecroding(Context context, String mobileNo, int isInComing) {
        String fileName = null;
        try {
            fileName =  AppUtils.createRecordFile(context,".amr").getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (callRecord == null) {
            callRecord = new CallRecord.Builder(context)
                    .setRecordFileName(fileName)
                    .setRecordDirName(AppConstants.STORAGE_DIR)
                    .setRecordDirPath(Environment.getExternalStorageDirectory().getPath()) // optional & default value
                    .setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT) // optional & default value
                    .setOutputFormat(MediaRecorder.OutputFormat.AMR_NB) // optional & default value
                    .setAudioSource(AppUtils.getIntegerPrefValue(context, AppUtils.AUDIO_SOURCE)) // optional & default value
                    .setShowSeed(true)// optional & default value ->Ex: RecordFileName_incoming.amr || RecordFileName_outgoing.amr
                    .build();
            CallRecorderInstance.getCallRecordInstance(context).setCallRecord(callRecord);
        }
        callRecord.enableSaveFile();
        //callRecord.startCallRecordService();
        callRecord.startCallReceiver();

        RecordedInfoModel record = getContactName(mobileNo, context);
        record.setFilePath(fileName);
        record.setIsFavourite("0");
        record.setIsInComing(isInComing);
        record.setIsCallEnd(0);
        record.setIsExcluded(false);
        record.setTime(System.currentTimeMillis());
        record.setIsOncloud(0);
        CallRecorderInstance.getCallRecordInstance(context).setRecordId(RecordedInfoDao.insert(record));
    }


}

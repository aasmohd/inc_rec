package com.app.xinger.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;


public class XingerContentProvider extends ContentProvider {

    private String TAG = XingerContentProvider.this.getClass().getSimpleName();
    private SQLiteDatabase db;
    private static final UriMatcher sUriMatcher;


    // set uri matcher
    static {
        sUriMatcher = new UriMatcher(0);
        sUriMatcher.addURI(XingerContentProviderData.AUTHORITY, RecordedInfoModel.class.getSimpleName(),
                XingerContentProviderData.ID_APP_TABLE);
        sUriMatcher.addURI(XingerContentProviderData.AUTHORITY, ExcludedContactModel.class.getSimpleName(),
                XingerContentProviderData.ID_EXCLUDED_TABLE);

    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case XingerContentProviderData.ID_APP_TABLE:
                return RecordedInfoModel.class.getSimpleName();
            case XingerContentProviderData.ID_EXCLUDED_TABLE:
                return ExcludedContactModel.class.getSimpleName();
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        db = XingerDBHelper.getdbInstance(getContext());
        db.beginTransaction();
        long result = 0;
        result = db.insert(getType(uri), null, values);
        db.setTransactionSuccessful();
        db.endTransaction();
        getContext().getContentResolver().notifyChange(uri, null);
        if (result == -1) {
            return null;
        }
        return Uri.withAppendedPath(uri, result + "");
    }

    @Override
    public boolean onCreate() {
        try {
            db = XingerDBHelper.getdbInstance(getContext());
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor resultCursor;
        db = XingerDBHelper.getdbInstance(getContext());
        resultCursor = db.query(getType(uri), projection, selection, selectionArgs, null, null, sortOrder);
        resultCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return resultCursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        db = XingerDBHelper.getdbInstance(getContext());
        int result = db.delete(getType(uri), selection, selectionArgs);
        Log.d(TAG, "deleted row :: " + result);
        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues values, String whereClause, String[] whereArgs) {
        int result = -1;
        String table = getType(uri);
        db = XingerDBHelper.getdbInstance(getContext());
        if (whereClause == null && values.get("_id") != null) {
            whereClause = "_id=?";
            whereArgs = new String[]{values.get("_id").toString()};
            Cursor c = db.query(table, new String[]{"_id"}, whereClause,
                    whereArgs, null, null, null, "1");
            if (c != null && c.getCount() > 0) {
                // update
                result = db.update(table, values, whereClause, whereArgs);
            } else {
                // insert
                insert(uri, values);
            }
            // close cursor here
            if (c != null) {
                c.close();
            }
        } else if ("*".equals(whereClause)) {
            whereClause = null;
            result = db.update(table, values, null, null);
        } else if (whereClause != null) {
            result = db.update(table, values, whereClause, whereArgs);
        } else {
            insert(uri, values);
        }
        if (result > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return result;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        int num = values == null ? -1 : values.length;
        int result = 0;
        db = XingerDBHelper.getdbInstance(getContext());
        db.beginTransaction();
        for (int i = 0; i < num; i++) {
            result += update(uri, values[i], null, null);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        getContext().getContentResolver().notifyChange(uri, null);
        return result;

    }

}

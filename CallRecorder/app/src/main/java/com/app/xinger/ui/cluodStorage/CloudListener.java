package com.app.xinger.ui.cluodStorage;

/**
 * Created by shubh on 05-Sep-17.
 */

public interface CloudListener {

    public void hideProgress();
    public void uploadFileOnServer();
    public void onLogout();
}


package com.app.xinger.util;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by tsingh on 21/1/15.
 */
public class AppUtil {
    private static final String LOG_TAG = AppUtil.class.getSimpleName();

    public static <T> T parseJson(String json, Class<T> tClass) {
        return new Gson().fromJson(json, tClass);
    }

    public static <T> T parseJson(JsonElement result, Class<T> tClass) {
        return new Gson().fromJson(result, tClass);
    }

    public static <T> T parseJson(String json, Type type) {
        return new Gson().fromJson(json, type);
    }

    public static JsonObject parseJson(String response) {
        JsonObject jo = null;
        JsonElement e = null;
        return new JsonParser().parse(response).getAsJsonObject();
    }

    public static JsonArray getJsonElement(String s) {
        return new JsonParser().parse(s).getAsJsonArray();
    }

    public static String getExt(String fileName) {
        String fileFormat = "";
        fileFormat = (fileName.split("[.]")[1]);
        return fileFormat;
    }

    public static String getJson(Object profile) {
        return new Gson().toJson(profile);
    }

    public static void showToast(Context mContext, String message) {
        if (mContext != null) {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Get Calendar Object. Pass time in milli second and required date format.
     * @param milliSeconds
     * @param dateFormat
     * @return
     */
    public static Calendar getCalendarObject(Long milliSeconds, String dateFormat) {
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            // Create a calendar object that will convert the date and time value in milliseconds to date.
            formatter.setTimeZone(TimeZone.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            return calendar;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
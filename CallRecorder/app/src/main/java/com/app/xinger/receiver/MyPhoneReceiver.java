package com.app.xinger.receiver;

import android.Manifest;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;

import com.app.xinger.cloud.UploadFileTask;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.ExcludedContactDao;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.util.DBUtil;
import com.app.xinger.utils.AppUtils;
import com.app.xinger.utils.CallRecorderInstance;
import com.app.xinger.utils.CustomNotification;
import com.app.xinger.utils.PermissionChecker;
import com.aykuttasil.callrecord.CallRecord;
import com.aykuttasil.callrecord.helper.PrefsHelper;
import com.aykuttasil.callrecord.receiver.PhoneCallReceiver;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class MyPhoneReceiver extends PhoneCallReceiver {

    private static CallRecord callRecord;
    //private static int numberofCall=0;
    private static MediaRecorder recorder;
    private File audiofile;
    private static boolean isRecordStarted = false;

    @Override
    protected void onIncomingCallReceived(Context context, String number, Date start) {
       // numberofCall++;
    }

    @Override
    protected void onIncomingCallAnswered(Context context, String number, Date start) {
        /*If available memory more then 100Mb then recording perform*/
        if (AppUtils.freeAvailableMemory()) {
            callRecord = CallRecorderInstance.getCallRecordInstance(context).getCallRecord();
            if (PermissionChecker.checkPermission(context,new String[]{
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
            })) {
                if (AppUtils.getBooleanPrefValue(context, AppUtils.ENABLE_CALL_RECORDER) &&
                        !ExcludedContactDao.isPhoneExist(AppUtils.normalizeNumber(context, number))&&
                        AppUtils.getBooleanPrefValue(context, AppUtils.ENABLE_INCOMING_CALLS)) {
                    if (callRecord == null)
                        startRecroding(context, number, 1);
                }
            }
        }

    }

    @Override
    protected void onIncomingCallEnded(Context context, String number, Date start, Date end) {
        //if (numberofCall==1){
            if (callRecord != null) {
                Cursor cursor=null;
                try {
                    cursor = RecordedInfoDao.getSelectedRecord(CallRecorderInstance.
                            getCallRecordInstance(context).getRecordId());
                    cursor.moveToFirst();
                    if (new File(cursor.getString(cursor.getColumnIndex("filePath"))).length()==0) {
                        RecordedInfoDao.deleteSelectedRecord(CallRecorderInstance.getCallRecordInstance(context).getRecordId());
                    }
                    else {
                        RecordedInfoModel model = new RecordedInfoModel();
                        model.set_id(CallRecorderInstance.getCallRecordInstance(context).getRecordId());
                        model.setIsCallEnd(1);
                        RecordedInfoDao.update(model);

                        // callRecord.stopCallReceiver();
                        if (AppUtils.getBooleanPrefValue(context,AppUtils.SHOW_NOTIFICATION)) {
                            new CustomNotification().showNotification(context, DBUtil.getFromContentValue(cursor
                                    , RecordedInfoModel.class));
                        }


                        if (AppUtils.getBooleanPrefValue(context,AppUtils.AUTO_SAVE_TO_CLOUD)){
                            new UploadFileTask(context,null,false);
                        }
                    }
                    cursor.close();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if (cursor!=null)
                        cursor.close();
                    CallRecorderInstance.getCallRecordInstance(context).destroy();
                    callRecord.disableSaveFile();
                    stopRecord(context);
                    callRecord = null;
                    //numberofCall=0;
                }
            }
       /* }
        else
            numberofCall--;*/
    }

    @Override
    protected void onOutgoingCallStarted(Context context, String number, Date start) {
        /*If available memory more then 100Mb then recording perform*/
        if (AppUtils.freeAvailableMemory()) {
           callRecord = CallRecorderInstance.getCallRecordInstance(context).getCallRecord();
           if (PermissionChecker.checkPermission(context,new String[]{
                   Manifest.permission.READ_CONTACTS,
                   Manifest.permission.CALL_PHONE,
                   Manifest.permission.WRITE_EXTERNAL_STORAGE,
                   Manifest.permission.RECORD_AUDIO
           })) {
               if (AppUtils.getBooleanPrefValue(context, AppUtils.ENABLE_CALL_RECORDER) &&
                       !ExcludedContactDao.isPhoneExist(AppUtils.normalizeNumber(context, number))&&
                       AppUtils.getBooleanPrefValue(context, AppUtils.ENABLE_OUTGOING_CALL)) {
                   if (callRecord == null)
                       startRecroding(context,AppUtils.normalizeNumber(context, number), 0);
               }
           }
       }
    }

    @Override
    protected void onOutgoingCallEnded(Context context, String number, Date start, Date end) {
        //if (numberofCall==1) {
            if (callRecord != null) {
                Cursor cursor=null;
                try {
                    cursor = RecordedInfoDao.getSelectedRecord(CallRecorderInstance.
                            getCallRecordInstance(context).getRecordId());
                    cursor.moveToFirst();
                    if (new File(cursor.getString(cursor.getColumnIndex("filePath"))).length()==0) {
                        RecordedInfoDao.deleteSelectedRecord(CallRecorderInstance.getCallRecordInstance(context).getRecordId());
                    }
                    else {
                        RecordedInfoModel model = new RecordedInfoModel();
                        model.set_id(CallRecorderInstance.getCallRecordInstance(context).getRecordId());
                        model.setIsCallEnd(1);
                        RecordedInfoDao.update(model);

                        //callRecord.stopCallReceiver();
                        if (AppUtils.getBooleanPrefValue(context,AppUtils.SHOW_NOTIFICATION)) {
                            new CustomNotification().showNotification(context, DBUtil.getFromContentValue(cursor
                                    , RecordedInfoModel.class));
                        }


                        if (AppUtils.getBooleanPrefValue(context,AppUtils.AUTO_SAVE_TO_CLOUD)){
                            new UploadFileTask(context,null,false);
                        }
                    }
                    cursor.close();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if (cursor!=null)
                        cursor.close();
                    CallRecorderInstance.getCallRecordInstance(context).destroy();
                    callRecord.disableSaveFile();
                    stopRecord(context);
                    callRecord = null;
                    //  numberofCall=0;
                }
            }
       /* }
        else
            numberofCall--;*/
    }

    @Override
    protected void onMissedCall(Context context, String number, Date start) {
        Log.e("MissedCall>>>","missed");
    }

    private void startRecroding(Context context, String mobileNo, int isInComing) {
        if (RecordedInfoDao.checkRecordCount().getCount()>=AppUtils.getIntegerPrefValue(context,AppUtils.CALL_LIMIT)) {
            RecordedInfoDao.deleteLeastRecord();
        }
        String fileName = null;
        try {
            fileName =  AppUtils.createRecordFile(context,".amr").getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (callRecord == null) {
            callRecord = new CallRecord.Builder(context)
                    .setRecordFileName(fileName)
                    .setRecordDirName(AppConstants.STORAGE_DIR)
                    .setRecordDirPath(Environment.getExternalStorageDirectory().getPath()) // optional & default value
                    .setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT) // optional & default value
                    .setOutputFormat(MediaRecorder.OutputFormat.AMR_NB) // optional & default value
                    .setAudioSource(MediaRecorder.AudioSource.MIC) // optional & default value//setAudioSource(AppUtils.getIntegerPrefValue(context, AppUtils.AUDIO_SOURCE))
                    .setShowSeed(true)// optional & default value ->Ex: RecordFileName_incoming.amr || RecordFileName_outgoing.amr
                    .build();
            CallRecorderInstance.getCallRecordInstance(context).setCallRecord(callRecord);
        }
        callRecord.enableSaveFile();
        RecordedInfoModel record = getContactName(mobileNo, context);
        startRecord(context,"",mobileNo);
        record.setFilePath(fileName);
        record.setIsFavourite("0");
        record.setIsInComing(isInComing);
        record.setIsCallEnd(0);
        record.setIsExcluded(false);
        record.setIsNew("1");
        record.setTime(System.currentTimeMillis());
        record.setIsOncloud(0);
        CallRecorderInstance.getCallRecordInstance(context).setRecordId(RecordedInfoDao.insert(record));
    }

    public RecordedInfoModel getContactName(final String phoneNumber, Context context) {
        //Uri uri= Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
                ContactsContract.CommonDataKinds.Phone._ID};
        RecordedInfoModel record = new RecordedInfoModel();
        String str = phoneNumber.substring(3,phoneNumber.length());
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                ContactsContract.CommonDataKinds.Phone.NUMBER +" LIKE ?",
                new String[]{"%"+str}, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                record.setContactName(cursor.getString(0));
                record.setProfilePic(cursor.getString(1));
                record.setContactId(cursor.getInt(2));
            } else {
                record.setContactName("");
                record.setProfilePic("");
                record.setContactId(0);
            }
            record.setMobileNo(phoneNumber);
            cursor.close();
        }
        return record;
    }

    private void startRecord(Context context, String seed, String phoneNumber) {
        try {
            boolean isSaveFile = PrefsHelper.readPrefBool(context, CallRecord.PREF_SAVE_FILE);
            Log.i(TAG, "isSaveFile: " + isSaveFile);

            // dosya kayıt edilsin mi?
            if (!isSaveFile) {
                return;
            }
            String file_name = PrefsHelper.readPrefString(context, CallRecord.PREF_FILE_NAME);
            String dir_path = PrefsHelper.readPrefString(context, CallRecord.PREF_DIR_PATH);
            String dir_name = PrefsHelper.readPrefString(context, CallRecord.PREF_DIR_NAME);
            boolean show_seed = PrefsHelper.readPrefBool(context, CallRecord.PREF_SHOW_SEED);
            boolean show_phone_number = PrefsHelper.readPrefBool(context, CallRecord.PREF_SHOW_PHONE_NUMBER);
            int output_format = PrefsHelper.readPrefInt(context, CallRecord.PREF_OUTPUT_FORMAT);
            int audio_source = PrefsHelper.readPrefInt(context, CallRecord.PREF_AUDIO_SOURCE);
            int audio_encoder = PrefsHelper.readPrefInt(context, CallRecord.PREF_AUDIO_ENCODER);

            File sampleDir = new File(dir_path + "/" + dir_name);

            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String suffix = "";
            switch (output_format) {
                case MediaRecorder.OutputFormat.AMR_NB: {
                    suffix = ".amr";
                    break;
                }
                case MediaRecorder.OutputFormat.AMR_WB: {
                    suffix = ".amr";
                    break;
                }
                case MediaRecorder.OutputFormat.MPEG_4: {
                    suffix = ".mp4";
                    break;
                }
                case MediaRecorder.OutputFormat.THREE_GPP: {
                    suffix = ".3gp";
                    break;
                }
                default: {
                    suffix = ".amr";
                    break;
                }
            }
            audiofile = new File(file_name);

            recorder = new MediaRecorder();
            recorder.setAudioSource(audio_source);
            recorder.setOutputFormat(output_format);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(audiofile.getAbsolutePath());

            recorder.prepare();
            //Thread.sleep(2000);
            recorder.start();
            isRecordStarted = true;
           // onRecordingStarted(context, callRecord, audiofile);

            Log.i(TAG, "record start");
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }


    private void stopRecord(Context context) {
        if (recorder != null && isRecordStarted) {

            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;

            isRecordStarted = false;
            //onRecordingFinished(context, callRecord, audiofile);

            Log.i(TAG, "record stop");
        }
    }


}

package com.app.xinger.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.homescreen.HomeScreenActivity;
import com.app.xinger.ui.recordingscreen.RecordingActivity;

/**
 * Created by shubh on 07-Sep-17.
 */

public class CustomNotification {

    public void showNotification(Context context, RecordedInfoModel record) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.custom_notification);
        contentView.setImageViewResource(R.id.img_app_icon, R.mipmap.ic_launcher);
        if (TextUtils.isEmpty(record.getContactName()))
            contentView.setTextViewText(R.id.txt_app_name,record.getMobileNo());
        else
            contentView.setTextViewText(R.id.txt_app_name,record.getContactName());

        contentView.setTextViewText(R.id.txt_time,AppUtils.getDate(record.getTime(),
                AppConstants.DATEFORMAT_HH_mm_a));
        Intent in =new Intent(context,HomeScreenActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,in,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setContent(contentView);

        NotificationManager mNotificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        contentView.setOnClickPendingIntent(R.id.txt_play, getPendingSelfIntent(context, record,false));
        contentView.setOnClickPendingIntent(R.id.txt_delete, getPendingSelfIntent(context,record,true));

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        mNotificationManager.notify(AppConstants.NOTIFICATION_ID, notification);
    }

    protected PendingIntent getPendingSelfIntent(Context context, RecordedInfoModel record, boolean isdelete) {
        Intent intent;
        if (isdelete)
            intent = new Intent(context, HomeScreenActivity.class);
        else
            intent = new Intent(context, RecordingActivity.class);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        intent.putExtra(AppConstants.RECORD_DATA,AppUtils.getGsonInstance().toJson(record));
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}

package com.app.xinger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;
import com.app.xinger.util.DBUtil;


/**
 * DB Manager class manage the table structure and db upgrade facility.
 *
 * @author Ashutosh
 */
public class XingerDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database
    // version.
    public static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "callRecorder.db";
    private static SQLiteOpenHelper dbManager;

    public XingerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should
     * happen.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // execute create table query
        String CREATE_TABLE_APP = DBUtil.getCreateTableStatement(RecordedInfoModel.class);
        String CREATE_TABLE_EXCLUDED = DBUtil.getCreateTableStatement(ExcludedContactModel.class);
        db.execSQL(CREATE_TABLE_APP);
        db.execSQL(CREATE_TABLE_EXCLUDED);
    }

    /**
     * Called when the database needs to be upgraded. The implementation should
     * use this method to drop tables, add tables, or do anything else it needs
     * to upgrade to the new schema version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        resetDB(db);
    }

    public void resetDB(SQLiteDatabase db) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + RecordedInfoModel.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + ExcludedContactModel.class.getSimpleName());
        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        resetDB(db);
    }

    /**
     * @param context
     * @return SQLiteDatabase instance.
     */
    public static SQLiteDatabase getdbInstance(Context context) {
        if (dbManager == null) {
            dbManager = new XingerDBHelper(context);
        }
        return dbManager.getWritableDatabase();
    }

    public static SQLiteDatabase getdbInstance() {
        return dbManager.getWritableDatabase();
    }

}

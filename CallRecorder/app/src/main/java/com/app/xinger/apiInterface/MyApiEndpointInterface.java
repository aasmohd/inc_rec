package com.app.xinger.apiInterface;


import com.app.xinger.service.UrlConfigurations;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Inflac on 02-11-2016.
 */
public interface MyApiEndpointInterface {

    /*registered new user*/
    @GET(UrlConfigurations.API_GET_BANNERS)
    Call<JsonObject> getBanners();

    /*forgot password*/
    @POST(UrlConfigurations.API_SENDMAIL)
    Call<JsonObject> sendEmail(@Body JsonObject object);

}


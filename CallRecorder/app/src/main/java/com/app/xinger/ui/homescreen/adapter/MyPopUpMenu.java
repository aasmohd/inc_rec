package com.app.xinger.ui.homescreen.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.FileProvider;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.app.xinger.R;
import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.ExcludedContactDao;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.ui.excludedscreen.ExcludedContactModel;
import com.app.xinger.utils.AppUtils;
import com.app.xinger.utils.PermissionChecker;

import java.io.File;

/**
 * Created by shubh on 30-Aug-17.
 */

public class MyPopUpMenu {

    public void ShowPopUp(final Context context, final RecordedInfoModel record, ImageView imageView) {

        PopupMenu popup = new PopupMenu(context, imageView);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.record_item_menu, popup.getMenu());
        if (record.getIsExcluded()) {
            popup.getMenu().getItem(4).setTitle(R.string.msg_remove_excluded);
        }
        else {
            popup.getMenu().getItem(4).setTitle(context.getString(R.string.add_to_excluded));
        }

        if (record.getContactName().equalsIgnoreCase("")) {
            popup.getMenu().removeItem(2);
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete :
                        AppUtils.ShowDeleteAlert(context,record,false);
                        break;

                    case R.id.action_share_recording :
                      /*  Uri uri = Uri.parse(record.getFilePath());
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("audio*//*");
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(share, context.getResources().getString(R.string.share_intent_msg)));*/

                        Uri uri = FileProvider.getUriForFile(context, AppConstants.FILEPROVIDER, new File(record.getFilePath()));
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("audio/*");
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(share, context.getResources().getString(R.string.share_intent_msg)));
                        break;

                    case R.id.action_details :
                        if (!record.getContactName().equalsIgnoreCase("")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData( Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI,
                                    String.valueOf(AppUtils.getContactIDFromNumber(record.getMobileNo(),context))));
                            context.startActivity(intent);
                        }
                        break;

                    case R.id.action_make_call :
                        if (PermissionChecker.checkPermission(context,new String[]{Manifest.permission.CALL_PHONE})) {
                            Intent intent1 = new Intent(Intent.ACTION_CALL);
                            intent1.setData(Uri.parse("tel:" + record.getMobileNo()));
                            context.startActivity(intent1);
                        }
                        else {
                            AppUtils.ShowAlert(context);
                        }
                        break;

                    case R.id.action_add_excluded :
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (record.getIsExcluded()) {
                                    RecordedInfoDao.updateExcluded(AppUtils.normalizeNumber(context,record.getMobileNo()),false);
                                    ExcludedContactDao.deleteSelectedContact(AppUtils.normalizeNumber(context,record.getMobileNo()));
                                }
                                else {
                                    RecordedInfoDao.updateExcluded(AppUtils.normalizeNumber(context,record.getMobileNo()),true);
                                    ExcludedContactModel model = new ExcludedContactModel();
                                    model.setMobileNo(AppUtils.normalizeNumber(context,
                                            AppUtils.normalizeNumber(context,record.getMobileNo())));
                                    model.setContactName(record.getContactName());
                                    ExcludedContactDao.insert(model);

                                }
                            }
                        },1);
                        break;
                }
                return true;
            }
        });

        popup.show();//showing popup menu

    }
}

package com.app.xinger.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by shubh on 31-Aug-17.
 */

public class PermissionChecker {

    public static final int REQUEST_CODE = 312;

    public static boolean checkPermission(Context context,String[] permission) {

        for (int i = 0; i < permission.length; i++) {
            if (ContextCompat.checkSelfPermission(context,permission[i])== PackageManager.PERMISSION_GRANTED) {
                continue;
            }
            else return false;
        }
        return true;
    }


    public static void requestPermission(Context context, String[] permission) {

        ActivityCompat.requestPermissions((Activity)context,
                permission,REQUEST_CODE);
    }

}

package com.app.xinger.ui.deleterecord;

import android.app.NotificationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.xinger.constants.AppConstants;
import com.app.xinger.db.dao.RecordedInfoDao;
import com.app.xinger.model.RecordedInfoModel;
import com.app.xinger.utils.AppUtils;

import java.io.File;

public class DeleteRecordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(AppConstants.NOTIFICATION_ID);

        try {
            RecordedInfoModel record = AppUtils.getGsonInstance().fromJson(getIntent().getStringExtra(AppConstants.RECORD_DATA),RecordedInfoModel.class);
            new File(record.getFilePath()).delete();
            RecordedInfoDao.deleteSelectedRecord(record.get_id());
            finish();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
